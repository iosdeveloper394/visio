import 'package:flutter/material.dart';

const primaryColor = "#2e2e2e";
const greyColor = "#404040";
const lightBlueColor = "#3e586a";
const backgroundColor = "#d2d6df";

Color hexToColor(String code) {
  return new Color(int.parse(code.substring(1, 7), radix: 16) + 0xFF000000);
}