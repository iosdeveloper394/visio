import 'package:flutter/material.dart';
import 'package:visio_app/Screens/Builder/BottomNav.dart';
import 'package:visio_app/Screens/Builder/BottomNav/BottomDescription.dart';
import 'package:visio_app/Screens/Builder/BottomNav/BottomHome.dart';
import 'package:visio_app/Screens/Builder/BottomNav/BottomSetting.dart';
import 'package:visio_app/Screens/Builder/BottomNav/BottomWind.dart';
import 'package:visio_app/Screens/Builder/DocumentScreens.dart';
import 'package:visio_app/Screens/Builder/InformationScreen.dart';
import 'package:visio_app/Screens/Builder/MilestoneScreen.dart';
import 'package:visio_app/Screens/Builder/ProjectDetail.dart';
import 'package:visio_app/Screens/Builder/WindDateDetail.dart';
import 'package:visio_app/Screens/Builder/WindDetailScreen.dart';
import 'package:visio_app/Screens/LoginScreen.dart';
import 'package:visio_app/Screens/OTPVerificationScreen.dart';
import 'package:visio_app/Screens/RegistrationPasswordScreen.dart';
import 'package:visio_app/Screens/RegistrationScreen.dart';
import 'package:visio_app/Screens/SplashScreen.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Visio App',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: SplashScreen(),
      initialRoute: "splash",
      routes: <String, WidgetBuilder>{
        "splash": (_) => SplashScreen(),
        "login": (_) => LoginScreen(),
        "registration": (_) => RegistrationScreen(),
        "builder_nav": (_) => BottomNav(index: 0,),
        "project_detail": (_) => ProjectDetail(),
        "wind_detail": (_) => WindDetailScreen(),
        "wind_date_detail": (_) => WindDateDetail(),
        "builder_home": (_) => BottomHome(),
        "builder_wind": (_) => BottomWind(),
        "builder_desc": (_) => BottomDescription(),
        "builder_setting": (_) => BottomSettings(),
        "builder_info": (_) => InformationScreen(),
        "builder_doc": (_) => DocumentScreen(),
        "builder_milestone": (_) => MilestoneScreen(),
      },
    );
  }
}
