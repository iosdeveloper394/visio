import 'package:flutter/material.dart';
import 'package:visio_app/Screens/ProjectManager/PaymentScreen.dart';
import 'package:visio_app/constant/constant.dart';

class PackageScreen extends StatefulWidget {
  bool isFromStart;
  PackageScreen({Key key, @required this.isFromStart}) : super(key: key);

  @override
  _PackageScreenState createState() => _PackageScreenState();
}

class _PackageScreenState extends State<PackageScreen> {
  int selectedIndex = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          decoration: BoxDecoration(
            color: hexToColor(primaryColor),
            image: DecorationImage(
              image: AssetImage("assets/images/background.png"),
              fit: BoxFit.cover,
            ),
          ),
          child: Padding(
            padding:
                const EdgeInsets.symmetric(horizontal: 20.0, vertical: 10.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(
                  height: 50.0,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    !widget.isFromStart
                        ? IconButton(
                            icon: Icon(
                              Icons.keyboard_arrow_left,
                              color: Colors.white,
                              size: 40.0,
                            ),
                            onPressed: () {
                              Navigator.pop(context);
                            })
                        : Container(),
                    Text(
                      "Pakete",
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 40.0,
                          fontWeight: FontWeight.bold),
                    ),
                  ],
                ),
                SizedBox(height: 20.0),
                Text(
                  "Bitte wählen Sie ein Paket",
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 25.0,
                  ),
                ),
                SizedBox(height: 30.0),
                Expanded(
                  child: Container(
                    child: ListView.separated(
                        separatorBuilder: (context, index) =>
                            SizedBox(width: 30.0),
                        shrinkWrap: true,
                        scrollDirection: Axis.horizontal,
                        itemCount: 3,
                        itemBuilder: (BuildContext context, int index) {
                          return index == 0
                              ? _buildBasicPackageView(index)
                              : index == 1
                                  ? _buildPremiumPackageView(index)
                                  : index == 2
                                      ? _buildEnterprisePackageView(index)
                                      : null;
                        }),
                  ),
                ),
                SizedBox(height: 40.0),
                Align(
                  alignment: FractionalOffset.bottomCenter,
                  child: Padding(
                    padding:
                        EdgeInsets.only(bottom: 50.0, right: 20.0, left: 20.0),
                    child: GestureDetector(
                      onTap: () {
                        widget.isFromStart ? moveToPaymentScreen(context) : Navigator.pop(context);
                      },
                      child: Container(
                        height: 50.0,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(8.0),
                          border: Border.all(
                            color: Colors.white,
                            width: 2.0,
                          ),
                        ),
                        child: Center(
                          child: Text(
                            "Anmeldung",
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 25.0,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  _buildBasicPackageView(int index) {
    return GestureDetector(
      onTap: () {
        setState(() {
          selectedIndex = index;
        });
      },
      child: Container(
        width: MediaQuery.of(context).size.width * 0.7,
        padding: const EdgeInsets.symmetric(horizontal: 15.0),
        decoration: BoxDecoration(
          color: hexToColor(greyColor).withOpacity(0.5),
          borderRadius: BorderRadius.circular(5.0),
          border: Border.all(
              color: selectedIndex == index ? Colors.blue : Colors.white,
              style: BorderStyle.solid,
              width: selectedIndex == index ? 4 : 2),
        ),
        child: MediaQuery.removePadding(
          context: context,
          removeTop: true,
          removeBottom: true,
          child: ListView(
            itemExtent: MediaQuery.of(context).size.height * 0.4,
            shrinkWrap: true,
            children: [
              Column(
                children: [
                  SizedBox(height: 20.0),
                  Center(
                    child: Text(
                      "Basic",
                      style: TextStyle(
                          color: selectedIndex == index
                              ? Colors.blue
                              : Colors.white,
                          fontSize: 30.0,
                          fontWeight: FontWeight.w500),
                    ),
                  ),
                  SizedBox(height: 20.0),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        "1 Zugang ",
                        style: TextStyle(color: Colors.white, fontSize: 19.0),
                      ),
                      Text(
                        "(1 Projekt)",
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 18.0,
                            fontWeight: FontWeight.w200),
                      ),
                    ],
                  ),
                  SizedBox(height: 20.0),
                  Center(
                    child: Text(
                      "Beobachtung des Bauprozesses",
                      textAlign: TextAlign.center,
                      style: TextStyle(color: Colors.white, fontSize: 18.0),
                    ),
                  ),
                  SizedBox(height: 20.0),
                  Center(
                    child: Text(
                      "Hinterlegen von Bauplänen und Dokumenten",
                      textAlign: TextAlign.center,
                      style: TextStyle(color: Colors.white, fontSize: 18.0),
                    ),
                  ),
                ],
              ),
              Column(
                children: [
                  SizedBox(height: 15.0),
                  Container(
                    height: 4,
                    decoration: BoxDecoration(
                      color:
                          selectedIndex == index ? Colors.blue : Colors.white,
                      borderRadius: BorderRadius.circular(5.0),
                    ),
                  ),
                  SizedBox(height: 10.0),
                  Center(
                    child: Text(
                      "Einrichtung - XXX€",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 18.0,
                          fontWeight: FontWeight.w300),
                    ),
                  ),
                  SizedBox(height: 10.0),
                  Center(
                    child: Text(
                      "monatlich - XXX€",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 18.0,
                          fontWeight: FontWeight.w300),
                    ),
                  ),
                  SizedBox(height: 15.0),
                  Container(
                    height: 50.0,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(8.0),
                      border: Border.all(
                        color:
                            selectedIndex == index ? Colors.blue : Colors.white,
                        width: 2.0,
                      ),
                    ),
                    child: Center(
                      child: Text(
                        selectedIndex == index ? "Gewählt" : "Wählen",
                        style: TextStyle(
                          color: selectedIndex == index
                              ? Colors.blue
                              : Colors.white,
                          fontSize: 18.0,
                          fontWeight: FontWeight.w300,
                        ),
                      ),
                    ),
                  ),
                  SizedBox(height: 15.0),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

  _buildPremiumPackageView(int index) {
    return GestureDetector(
      onTap: () {
        setState(() {
          selectedIndex = index;
        });
      },
      child: Container(
        width: MediaQuery.of(context).size.width * 0.7,
        padding: const EdgeInsets.symmetric(horizontal: 15.0),
        decoration: BoxDecoration(
          color: hexToColor(greyColor).withOpacity(0.5),
          borderRadius: BorderRadius.circular(5.0),
          border: Border.all(
              color: selectedIndex == index ? Colors.blue : Colors.white,
              style: BorderStyle.solid,
              width: selectedIndex == index ? 4 : 2),
        ),
        child: MediaQuery.removePadding(
          context: context,
          removeTop: true,
          child: ListView(
            itemExtent: MediaQuery.of(context).size.height * 0.4,
            shrinkWrap: true,
            children: [
              Column(
                children: [
                  SizedBox(height: 20.0),
                  Center(
                    child: Text(
                      "Premium",
                      style: TextStyle(
                          color: selectedIndex == index
                              ? Colors.blue
                              : Colors.white,
                          fontSize: 30.0,
                          fontWeight: FontWeight.w500),
                    ),
                  ),
                  SizedBox(height: 20.0),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        "4 Zugänge ",
                        style: TextStyle(color: Colors.white, fontSize: 19.0),
                      ),
                      Text(
                        "(2 Projekt)",
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 18.0,
                            fontWeight: FontWeight.w200),
                      ),
                    ],
                  ),
                  SizedBox(height: 20.0),
                  Center(
                    child: Text(
                      "Beobachtung des Bauprozesses",
                      textAlign: TextAlign.center,
                      style: TextStyle(color: Colors.white, fontSize: 18.0),
                    ),
                  ),
                  SizedBox(height: 20.0),
                  Center(
                    child: Text(
                      "Hinterlegen von Bauplänen und Dokumenten",
                      textAlign: TextAlign.center,
                      style: TextStyle(color: Colors.white, fontSize: 18.0),
                    ),
                  ),
                  SizedBox(height: 20.0),
                  RichText(
                    textAlign: TextAlign.center,
                    text: new TextSpan(
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 18.0,
                          height: 1.4,
                          fontWeight: FontWeight.w200),
                      children: <TextSpan>[
                        new TextSpan(text: 'Wettermonitoring '),
                        new TextSpan(
                            text: 'Basic',
                            style: new TextStyle(fontWeight: FontWeight.w400)),
                        TextSpan(
                          text: ' (Zugriff auf wochengenaue Wetterdaten).',
                        ),
                      ],
                    ),
                  ),
                ],
              ),
              Column(
                children: [
                  SizedBox(height: 15.0),
                  Container(
                    height: 4,
                    decoration: BoxDecoration(
                      color:
                          selectedIndex == index ? Colors.blue : Colors.white,
                      borderRadius: BorderRadius.circular(5.0),
                    ),
                  ),
                  SizedBox(height: 10.0),
                  Center(
                    child: Text(
                      "Einrichtung - XXX€",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 18.0,
                          fontWeight: FontWeight.w300),
                    ),
                  ),
                  SizedBox(height: 10.0),
                  Center(
                    child: Text(
                      "monatlich - XXX€",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 18.0,
                          fontWeight: FontWeight.w300),
                    ),
                  ),
                  SizedBox(height: 15.0),
                  Container(
                    height: 50.0,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(8.0),
                      border: Border.all(
                        color:
                            selectedIndex == index ? Colors.blue : Colors.white,
                        width: 2.0,
                      ),
                    ),
                    child: Center(
                      child: Text(
                        selectedIndex == index ? "Gewählt" : "Wählen",
                        style: TextStyle(
                          color: selectedIndex == index
                              ? Colors.blue
                              : Colors.white,
                          fontSize: 18.0,
                          fontWeight: FontWeight.w300,
                        ),
                      ),
                    ),
                  ),
                  SizedBox(height: 15.0),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

  _buildEnterprisePackageView(int index) {
    return GestureDetector(
      onTap: () {
        setState(() {
          selectedIndex = index;
        });
      },
      child: Container(
        width: MediaQuery.of(context).size.width * 0.7,
        padding: const EdgeInsets.symmetric(horizontal: 15.0),
        decoration: BoxDecoration(
          color: hexToColor(greyColor).withOpacity(0.5),
          borderRadius: BorderRadius.circular(5.0),
          border: Border.all(
              color: selectedIndex == index ? Colors.blue : Colors.white,
              style: BorderStyle.solid,
              width: selectedIndex == index ? 4 : 2),
        ),
        child: MediaQuery.removePadding(
          context: context,
          removeTop: true,
          child: ListView(
            itemExtent: MediaQuery.of(context).size.height * 0.4,
            shrinkWrap: true,
            children: [
              Column(
                children: [
                  SizedBox(height: 20.0),
                  Center(
                    child: Text(
                      "Enterprise",
                      style: TextStyle(
                          color: selectedIndex == index
                              ? Colors.blue
                              : Colors.white,
                          fontSize: 30.0,
                          fontWeight: FontWeight.w500),
                    ),
                  ),
                  SizedBox(height: 20.0),
                  RichText(
                    textAlign: TextAlign.center,
                    text: new TextSpan(
                      style: TextStyle(color: Colors.white, fontSize: 19.0),
                      children: <TextSpan>[
                        new TextSpan(text: 'Unbegrenzte Zugänge '),
                        new TextSpan(
                            text: '\n(4 Projekt)',
                            style: new TextStyle(
                                color: Colors.white,
                                fontSize: 18.0,
                                fontWeight: FontWeight.w200)),
                      ],
                    ),
                  ),
                  SizedBox(height: 20.0),
                  Center(
                    child: Text(
                      "Beobachtung des Bauprozesses",
                      textAlign: TextAlign.center,
                      style: TextStyle(color: Colors.white, fontSize: 18.0),
                    ),
                  ),
                  SizedBox(height: 20.0),
                  Center(
                    child: Text(
                      "Hinterlegen von Bauplänen und Dokumenten",
                      textAlign: TextAlign.center,
                      style: TextStyle(color: Colors.white, fontSize: 18.0),
                    ),
                  ),
                  SizedBox(height: 20.0),
                  RichText(
                    textAlign: TextAlign.center,
                    text: new TextSpan(
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 17.0,
                          height: 1.2,
                          fontWeight: FontWeight.w200),
                      children: <TextSpan>[
                        new TextSpan(text: 'Wettermonitoring '),
                        new TextSpan(
                            text: 'Advanced',
                            style: new TextStyle(fontWeight: FontWeight.w400)),
                        TextSpan(
                          text: ' (Zugriff auf tagesgenaue Wetterdaten).',
                        ),
                      ],
                    ),
                  ),
                ],
              ),
              Column(
                children: [
                  SizedBox(height: 15.0),
                  Container(
                    height: 4,
                    decoration: BoxDecoration(
                      color:
                          selectedIndex == index ? Colors.blue : Colors.white,
                      borderRadius: BorderRadius.circular(5.0),
                    ),
                  ),
                  SizedBox(height: 10.0),
                  Center(
                    child: Text(
                      "Einrichtung - XXX€",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 18.0,
                          fontWeight: FontWeight.w300),
                    ),
                  ),
                  SizedBox(height: 10.0),
                  Center(
                    child: Text(
                      "monatlich - XXX€",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 18.0,
                          fontWeight: FontWeight.w300),
                    ),
                  ),
                  SizedBox(height: 15.0),
                  Container(
                    height: 50.0,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(8.0),
                      border: Border.all(
                        color:
                            selectedIndex == index ? Colors.blue : Colors.white,
                        width: 2.0,
                      ),
                    ),
                    child: Center(
                      child: Text(
                        selectedIndex == index ? "Gewählt" : "Wählen",
                        style: TextStyle(
                          color: selectedIndex == index
                              ? Colors.blue
                              : Colors.white,
                          fontSize: 18.0,
                          fontWeight: FontWeight.w300,
                        ),
                      ),
                    ),
                  ),
                  SizedBox(height: 15.0),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

  moveToPaymentScreen(BuildContext context) {
    Navigator.of(context).pushReplacement(
        MaterialPageRoute(builder: (context) => PaymentScreen()));
  }
}
