import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';
import 'package:visio_app/Screens/ProjectManager/AdminBottomNav/AdminBottomHome.dart';
import 'package:visio_app/Screens/ProjectManager/AdminBottomNav/AdminBottomSetting.dart';
import 'package:visio_app/Screens/ProjectManager/AdminBottomNav/AdminBottomWind.dart';
import 'package:visio_app/constant/constant.dart';

class AdminBottomNav extends StatefulWidget {
  const AdminBottomNav({Key key}) : super(key: key);

  @override
  _AdminBottomNavState createState() => _AdminBottomNavState();
}

class _AdminBottomNavState extends State<AdminBottomNav> {
  int _currentIndex = 0;
  PersistentTabController _controller;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    // widget.index != null ? _currentIndex = widget.index : print("test");
    _controller = PersistentTabController(initialIndex: 0);
  }

  List<PersistentBottomNavBarItem> _navBarsItems() {
    return [
      PersistentBottomNavBarItem(
        icon: Icon(CupertinoIcons.home),
        title: ("Home"),
        activeColorPrimary: CupertinoColors.white,
        inactiveColorPrimary: CupertinoColors.systemGrey,
      ),
      PersistentBottomNavBarItem(
        icon: Icon(CupertinoIcons.wind),
        title: ("Settings"),
        activeColorPrimary: CupertinoColors.white,
        inactiveColorPrimary: CupertinoColors.systemGrey,
      ),
      PersistentBottomNavBarItem(
        icon: Icon(CupertinoIcons.settings),
        title: ("Settings"),
        activeColorPrimary: CupertinoColors.white,
        inactiveColorPrimary: CupertinoColors.systemGrey,
      ),
    ];
  }

  @override
  Widget build(BuildContext context) {
    return PersistentTabView(
      context,
      controller: _controller,
      screens: [
        AdminBottomHome(),
        AdminBottomWind(),
        AdminBottomSetting(),
      ],
      items: _navBarsItems(),
      confineInSafeArea: true,
      backgroundColor: hexToColor(primaryColor), // Default is Colors.white.
      handleAndroidBackButtonPress: true, // Default is true.
      resizeToAvoidBottomInset:
          true, // This needs to be true if you want to move up the screen when keyboard appears. Default is true.
      stateManagement: true, // Default is true.
      hideNavigationBarWhenKeyboardShows:
          true, // Recommended to set 'resizeToAvoidBottomInset' as true while using this argument. Default is true.
      decoration: NavBarDecoration(
        borderRadius: BorderRadius.circular(10.0),
        colorBehindNavBar: Colors.transparent,
      ),
      popAllScreensOnTapOfSelectedTab: true,
      popActionScreens: PopActionScreensType.all,
      itemAnimationProperties: ItemAnimationProperties(
        // Navigation Bar's items animation properties.
        duration: Duration(milliseconds: 200),
        curve: Curves.ease,
      ),
      screenTransitionAnimation: ScreenTransitionAnimation(
        // Screen transition animation on change of selected tab.
        animateTabTransition: true,
        curve: Curves.ease,
        duration: Duration(milliseconds: 200),
      ),
      navBarStyle:
          NavBarStyle.style12, // Choose the nav bar style with this property.
    );
    // DefaultTabController(
    //   length: 4,
    //   initialIndex: _currentIndex,
    //   child: Scaffold(
    //     extendBody: true,
    //     bottomNavigationBar: menu(context),
    //     body: TabBarView(
    //       physics: NeverScrollableScrollPhysics(),
    //       children: [
    // BottomHome(),
    // BottomWind(),
    // BottomDescription(),
    // BottomSettings(),
    //       ],
    //     ),
    //   ),
    // );
  }
}
