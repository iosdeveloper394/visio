import 'package:flutter/material.dart';
import 'package:visio_app/Screens/ProjectManager/AboutUsScreen.dart';
import 'package:visio_app/Screens/ProjectManager/PackageScreen.dart';

class AdminBottomSetting extends StatefulWidget {
  const AdminBottomSetting({Key key}) : super(key: key);

  @override
  _AdminBottomSettingState createState() => _AdminBottomSettingState();
}

class _AdminBottomSettingState extends State<AdminBottomSetting> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage("assets/images/background.png"),
            fit: BoxFit.cover,
          ),
        ),
        child: Padding(
          padding: const EdgeInsets.only(
              top: 15.0, left: 15.0, right: 15.0, bottom: 10.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(
                height: 50.0,
              ),
              Text(
                "Setting",
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 30.0,
                    fontWeight: FontWeight.bold),
              ),
              SizedBox(
                height: 50.0,
              ),
              Expanded(
                child: Container(
                  child: ListView.separated(
                      separatorBuilder: (context, index) =>
                          SizedBox(height: 10.0),
                      physics: BouncingScrollPhysics(),
                      padding: EdgeInsets.symmetric(vertical: 5.0),
                      scrollDirection: Axis.vertical,
                      shrinkWrap: true,
                      itemCount: 3,
                      itemBuilder: (BuildContext context, int index) {
                        return _buildView(index);
                      }),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  _buildView(int index) {
    return Container(
      child: FlatButton(
        onPressed: () {
          moveToDynamicScreen(context, index);
        },
        child: Column(
          children: [
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  index == 0 ? "My Package" : index == 1 ? "Buy Package" : index == 2 ? "About Us" : "",
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 20.0,
                      fontWeight: FontWeight.w500),
                ),
                Icon(
                  Icons.arrow_forward_ios,
                  color: Colors.white,
                  size: 20.0,
                ),
              ],
            ),
            SizedBox(height: 5.0),
            Container(
              height: 1,
              color: Colors.white,
            )
          ],
        ),
      ),
    );
  }

  moveToDynamicScreen(BuildContext context, int index) {

    if(index == 0) {
      // Navigator.of(context)
      //   .push(MaterialPageRoute(builder: (context) => WindDetailScreen()));
    } else if(index == 1){
      Navigator.of(context)
        .push(MaterialPageRoute(builder: (context) => PackageScreen(isFromStart: false,)));
    } else {
      Navigator.of(context)
        .push(MaterialPageRoute(builder: (context) => AboutUsScreen()));
    }

  }
}
