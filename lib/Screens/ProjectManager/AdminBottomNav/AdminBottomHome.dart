import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:visio_app/Screens/ProjectManager/AddProjectScreen.dart';

class AdminBottomHome extends StatefulWidget {
  const AdminBottomHome({Key key}) : super(key: key);

  @override
  _AdminBottomHomeState createState() => _AdminBottomHomeState();
}

class _AdminBottomHomeState extends State<AdminBottomHome> {
  final itemCount = 4;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage("assets/images/background.png"),
            fit: BoxFit.cover,
          ),
        ),
        child: Padding(
          padding: const EdgeInsets.only(
              top: 15.0, left: 15.0, right: 15.0, bottom: 10.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(
                height: 50.0,
              ),
              Text(
                "Projektbereich",
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 40.0,
                    fontWeight: FontWeight.bold),
              ),
              SizedBox(
                height: 10.0,
              ),
              Text(
                "Übersicht über alle Projekte",
                style: TextStyle(
                    color: Colors.white38,
                    fontSize: 25.0,
                    fontWeight: FontWeight.w300),
              ),
              SizedBox(
                height: 10.0,
              ),
              Expanded(
                child: Container(
                  child: ListView.separated(
                      separatorBuilder: (context, index) =>
                          SizedBox(height: 15.0),
                      physics: BouncingScrollPhysics(),
                      padding: EdgeInsets.symmetric(vertical: 10.0),
                      scrollDirection: Axis.vertical,
                      shrinkWrap: true,
                      itemCount: itemCount,
                      itemBuilder: (BuildContext context, int index) {
                        return _buildProjectView(index);
                      }),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  _buildProjectView(int index) {
    return ((index == itemCount - 1))
        ? GestureDetector(
            onTap: () {
              moveToAddProjectScreen(context);
            },
            child: new Align(
                alignment: Alignment.center,
                child: Container(
                  margin: EdgeInsets.all(10),
                  padding: EdgeInsets.all(10),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(100),
                      border: Border.all(width: 4, color: Colors.white)),
                  child: Icon(
                    Icons.add,
                    color: Colors.white,
                  ),
                )),
          )
        : Container(
            height: 280,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(20),
              color: Colors.white,
            ),
            child: Column(
              children: [
                Container(
                  height: 200,
                  width: MediaQuery.of(context).size.width,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20),
                    color: Colors.blue,
                  ),
                  child: Image.asset(
                    "assets/images/Logo1.png",
                    height: 150.0,
                    fit: BoxFit.cover,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(15.0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "LettestraBe 2",
                            style: TextStyle(
                                color: Colors.black,
                                fontSize: 18.0,
                                fontWeight: FontWeight.bold),
                          ),
                          Row(
                            children: [
                              GestureDetector(
                                onTap: () {},
                                child: Icon(
                                  CupertinoIcons.cube,
                                  size: 30,
                                  color: Colors.black,
                                ),
                              ),
                              SizedBox(width: 10.0),
                              GestureDetector(
                                onTap: () {},
                                child: Icon(
                                  Icons.edit,
                                  color: Colors.black,
                                  size: 30.0,
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                      Text(
                        "Progress: 65%",
                        style: TextStyle(
                            color: Colors.grey,
                            fontSize: 16.0,
                            fontWeight: FontWeight.w300),
                      ),
                    ],
                  ),
                )
              ],
            ),
          );
  }

  moveToAddProjectScreen(BuildContext context) {
    Navigator.of(context)
        .push(MaterialPageRoute(builder: (context) => AddProjectScreen()));
  }
}
