import 'package:flutter/material.dart';
import 'package:visio_app/Screens/ProjectManager/VerificationScreen.dart';
import 'package:visio_app/constant/constant.dart';

class PaymentScreen extends StatefulWidget {
  @override
  _PaymentScreenState createState() => _PaymentScreenState();
}

class _PaymentScreenState extends State<PaymentScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage("assets/images/background.png"),
              fit: BoxFit.cover,
            ),
          ),
          child: Padding(
            padding:
                const EdgeInsets.symmetric(horizontal: 20.0, vertical: 10.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(
                  height: 50.0,
                ),
                Text(
                  "Zalung",
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 60.0,
                      fontWeight: FontWeight.bold),
                ),
                SizedBox(height: 15.0),
                RichText(
                  textAlign: TextAlign.left,
                  text: new TextSpan(
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 25.0,
                    ),
                    children: <TextSpan>[
                      new TextSpan(text: 'Sie haben das Paket '),
                      new TextSpan(
                          text: 'Premium',
                          style: new TextStyle(
                              fontWeight: FontWeight.bold, color: Colors.blue)),
                      TextSpan(
                        text: ' gewählt.',
                      ),
                    ],
                  ),
                ),
                SizedBox(height: 25.0),
                RichText(
                  textAlign: TextAlign.left,
                  text: new TextSpan(
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 25.0,
                    ),
                    children: <TextSpan>[
                      new TextSpan(
                          text: 'Bitte zahlen Sie die Einrichtungsgebühr von '),
                      new TextSpan(
                          text: '\nXXX Euro',
                          style: new TextStyle(
                              fontWeight: FontWeight.bold,
                              color: Colors.white)),
                      TextSpan(
                        text: ' auf das folgende Konto.',
                      ),
                    ],
                  ),
                ),
                SizedBox(height: 15.0),
                Container(
                  padding: const EdgeInsets.all(10.0),
                  width: MediaQuery.of(context).size.width,
                  decoration: BoxDecoration(
                    color: hexToColor(primaryColor),
                    borderRadius: BorderRadius.circular(10.0),
                    border: Border.all(color: Colors.white, width: 1),
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text("STS GmbH",
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 25.0,
                              fontWeight: FontWeight.w300)),
                      SizedBox(height: 10.0),
                      Text("Deutsche Bank",
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 20.0,
                              fontWeight: FontWeight.w300)),
                      SizedBox(height: 10.0),
                      Text("DEXX XXXX XXXX XXXX XXXX XX",
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 20.0,
                              fontWeight: FontWeight.w300)),
                      SizedBox(height: 10.0),
                      Text("BYLADEM1001",
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 20.0,
                              fontWeight: FontWeight.w300)),
                    ],
                  ),
                ),
                SizedBox(height: 20.0),
                RichText(
                  textAlign: TextAlign.left,
                  text: new TextSpan(
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 21.0,
                        fontWeight: FontWeight.w300),
                    children: <TextSpan>[
                      new TextSpan(text: 'Die monatliche Zahlung in Höhe von '),
                      new TextSpan(
                          text: 'XX Euro',
                          style: new TextStyle(
                              fontWeight: FontWeight.w500,
                              color: Colors.white)),
                      TextSpan(
                        text: ' wird per Lastschrift von ihrem Konto abgebucht',
                      ),
                    ],
                  ),
                ),
                Expanded(
                  child: Align(
                    alignment: FractionalOffset.bottomCenter,
                    child: Padding(
                      padding: EdgeInsets.only(bottom: 60.0),
                      child: GestureDetector(
                        onTap: () {
                          moveToVerificationScreen(context);
                        },
                        child: Container(
                          height: 50.0,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(8.0),
                            border: Border.all(
                              color: Colors.white,
                              width: 2.0,
                            ),
                          ),
                          child: Center(
                            child: Text(
                              "Weiter",
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 25.0,
                                fontWeight: FontWeight.w500,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  moveToVerificationScreen(BuildContext context) {
    Navigator.of(context)
        .pushReplacement(MaterialPageRoute(builder: (context) => VerificationScreen()));
  }
}
