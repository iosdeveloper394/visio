import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:visio_app/Screens/OTPVerificationScreen.dart';
import 'package:visio_app/constant/constant.dart';

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final passwordController = TextEditingController();
  final emailTextController = TextEditingController();
  var isProjectManager = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage("assets/images/background.png"),
              fit: BoxFit.cover,
            ),
          ),
          child: Padding(
            padding: const EdgeInsets.all(15.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(
                  height: 50.0,
                ),
                Text(
                  "Login",
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 40.0,
                      fontWeight: FontWeight.bold),
                ),
                SizedBox(height: 10.0),
                Text(
                  "Bitte melden Sie sich an",
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 25.0,
                  ),
                ),
                SizedBox(height: 50.0),
                Column(
                  children: [
                    Container(
                      child: Column(
                        children: [
                          Container(
                            child: TextField(
                              // onChanged: (value) => isFieldsValid(),
                              controller: emailTextController,
                              keyboardType: TextInputType.text,
                              decoration: InputDecoration(
                                prefixIcon: Icon(
                                  CupertinoIcons.mail,
                                  color: Colors.white,
                                ),
                                hintStyle: new TextStyle(
                                  color: Colors.grey,
                                  fontSize: 18.0,
                                ),
                                enabledBorder: UnderlineInputBorder(
                                  borderSide: BorderSide(
                                      color: Colors.white, width: 2.0),
                                ),
                                focusedBorder: UnderlineInputBorder(
                                  borderSide: BorderSide(
                                      color: Colors.white, width: 2.0),
                                ),
                                hintText: 'E-Mail Adresse',
                              ),
                              style: TextStyle(
                                fontSize: 14.0,
                                color: Colors.white,
                              ),
                            ),
                          ),
                          SizedBox(height: 30.0),
                          Container(
                            child: TextField(
                              // onChanged: (value) => isFieldsValid(),
                              controller: passwordController,
                              keyboardType: TextInputType.text,
                              obscureText: true,
                              decoration: InputDecoration(
                                prefixIcon: Icon(
                                  CupertinoIcons.lock_fill,
                                  color: Colors.white,
                                ),
                                hintStyle: new TextStyle(
                                  color: Colors.grey,
                                  fontSize: 18.0,
                                ),
                                enabledBorder: UnderlineInputBorder(
                                  borderSide: BorderSide(
                                      color: Colors.white, width: 2.0),
                                ),
                                focusedBorder: UnderlineInputBorder(
                                  borderSide: BorderSide(
                                      color: Colors.white, width: 2.0),
                                ),
                                hintText: 'Passwort',
                              ),
                              style: TextStyle(
                                fontSize: 14.0,
                                color: Colors.white,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(height: 50.0),
                    Container(
                      height: 40.0,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(8.0),
                        border: Border.all(
                          color: Colors.white,
                          width: 2.0,
                        ),
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          GestureDetector(
                            onTap: () {
                              setState(() {
                                isProjectManager = false;
                              });
                            },
                            child: Container(
                              width: MediaQuery.of(context).size.width / 2 - 18,
                              decoration: BoxDecoration(
                                borderRadius: const BorderRadius.only(
                                  topLeft: Radius.circular(8.0),
                                  bottomLeft: Radius.circular(8.0),
                                ),
                                color: !isProjectManager
                                    ? hexToColor(lightBlueColor)
                                    : Colors.transparent,
                              ),
                              child: Center(
                                child: Text(
                                  "Bauherr",
                                  style: TextStyle(
                                    color: Colors.white,
                                  ),
                                ),
                              ),
                            ),
                          ),
                          Container(
                            height: 40.0,
                            width: 2.0,
                            color: Colors.white,
                          ),
                          GestureDetector(
                            onTap: () {
                              setState(() {
                                isProjectManager = true;
                              });
                            },
                            child: Container(
                              width: MediaQuery.of(context).size.width / 2 - 18,
                              decoration: BoxDecoration(
                                borderRadius: const BorderRadius.only(
                                  topRight: Radius.circular(6.0),
                                  bottomRight: Radius.circular(6.0),
                                ),
                                color: isProjectManager
                                    ? hexToColor(lightBlueColor)
                                    : Colors.transparent,
                              ),
                              child: Center(
                                child: Text(
                                  "Projektmanager",
                                  style: TextStyle(
                                    color: Colors.white,
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(height: 20.0),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text(
                          'Sie haben noch keinen Account?',
                          style: TextStyle(
                            color: Colors.white60,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        SizedBox(width: 2.0),
                        GestureDetector(
                          onTap: () {
                            moveToRegistrationScreen(context);
                          },
                          child: Text(
                            'Registrieren',
                            style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
                Expanded(
                  child: Align(
                    alignment: FractionalOffset.bottomCenter,
                    child: Padding(
                      padding: EdgeInsets.only(bottom: 100.0),
                      child: GestureDetector(
                        onTap: () {
                          moveToOTPScreen(context);
                        },
                        child: Container(
                          height: 50.0,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(8.0),
                            border: Border.all(
                              color: Colors.white,
                              width: 2.0,
                            ),
                          ),
                          child: Center(
                            child: Text(
                              "Anmeldung",
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 25.0,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  moveToOTPScreen(BuildContext context) {
    Navigator.of(context)
        .push(MaterialPageRoute(builder: (context) => OTPVerificationScreen(text: "Rüdiger", isProjectManager: isProjectManager,)));
  }

  moveToRegistrationScreen(BuildContext context) {
    Navigator.pushNamed(context, 'registration');
  }
}
