import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:visio_app/Screens/RegistrationPasswordScreen.dart';
import 'package:visio_app/constant/constant.dart';

class RegistrationScreen extends StatefulWidget {
  @override
  _RegistrationScreenState createState() => _RegistrationScreenState();
}

class _RegistrationScreenState extends State<RegistrationScreen> {
  final firstnameTextController = TextEditingController();
  final lastnameTextController = TextEditingController();
  final emailTextController = TextEditingController();
  var isProjectManager = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage("assets/images/background.png"),
              fit: BoxFit.cover,
            ),
          ),
          child: Padding(
            padding: const EdgeInsets.all(15.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(height: 50.0,),
                Text(
                  "Register",
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 40.0,
                      fontWeight: FontWeight.bold),
                ),
                SizedBox(height: 10.0),
                Text(
                  "Bitte registrieren Sie sich",
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 25.0,
                  ),
                ),
                SizedBox(height: 50.0),
                Column(
                  children: [
                    Container(
                      child: Column(
                        children: [
                          Container(
                            child: TextField(
                              // onChanged: (value) => isFieldsValid(),
                              controller: firstnameTextController,
                              keyboardType: TextInputType.text,
                              decoration: InputDecoration(
                                prefixIcon: Icon(
                                  CupertinoIcons.person_fill,
                                  color: Colors.white,
                                ),
                                hintStyle: new TextStyle(
                                  color: Colors.grey,
                                  fontSize: 18.0,
                                ),
                                enabledBorder: UnderlineInputBorder(
                                  borderSide: BorderSide(
                                      color: Colors.white, width: 2.0),
                                ),
                                focusedBorder: UnderlineInputBorder(
                                  borderSide: BorderSide(
                                      color: Colors.white, width: 2.0),
                                ),
                                hintText: 'Vorname',
                              ),
                              style: TextStyle(
                                fontSize: 14.0,
                                color: Colors.white,
                              ),
                            ),
                          ),
                          SizedBox(height: 20.0),
                          Container(
                            child: TextField(
                              // onChanged: (value) => isFieldsValid(),
                              controller: lastnameTextController,
                              keyboardType: TextInputType.text,
                              decoration: InputDecoration(
                                prefixIcon: Icon(
                                  CupertinoIcons.person_fill,
                                  color: Colors.white,
                                ),
                                hintStyle: new TextStyle(
                                  color: Colors.grey,
                                  fontSize: 18.0,
                                ),
                                enabledBorder: UnderlineInputBorder(
                                  borderSide: BorderSide(
                                      color: Colors.white, width: 2.0),
                                ),
                                focusedBorder: UnderlineInputBorder(
                                  borderSide: BorderSide(
                                      color: Colors.white, width: 2.0),
                                ),
                                hintText: 'Nachname',
                              ),
                              style: TextStyle(
                                fontSize: 14.0,
                                color: Colors.white,
                              ),
                            ),
                          ),
                          SizedBox(height: 20.0),
                          Container(
                            child: TextField(
                              // onChanged: (value) => isFieldsValid(),
                              controller: emailTextController,
                              keyboardType: TextInputType.text,
                              decoration: InputDecoration(
                                prefixIcon: Icon(
                                  CupertinoIcons.mail,
                                  color: Colors.white,
                                ),
                                hintStyle: new TextStyle(
                                  color: Colors.grey,
                                  fontSize: 18.0,
                                ),
                                enabledBorder: UnderlineInputBorder(
                                  borderSide: BorderSide(
                                      color: Colors.white, width: 2.0),
                                ),
                                focusedBorder: UnderlineInputBorder(
                                  borderSide: BorderSide(
                                      color: Colors.white, width: 2.0),
                                ),
                                hintText: 'E-Mail Adresse',
                              ),
                              style: TextStyle(
                                fontSize: 14.0,
                                color: Colors.white,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(height: 50.0),
                    Container(
                      height: 40.0,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(8.0),
                        border: Border.all(
                          color: Colors.white,
                          width: 2.0,
                        ),
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          GestureDetector(
                            onTap: () {
                              setState(() {
                                isProjectManager = false;
                              });
                            },
                            child: Container(
                              width: MediaQuery.of(context).size.width / 2 - 18,
                              decoration: BoxDecoration(
                                borderRadius: const BorderRadius.only(
                                  topLeft: Radius.circular(8.0),
                                  bottomLeft: Radius.circular(8.0),
                                ),
                                color: !isProjectManager
                                    ? hexToColor(lightBlueColor)
                                    : Colors.transparent,
                              ),
                              child: Center(
                                child: Text(
                                  "Bauherr",
                                  style: TextStyle(
                                    color: Colors.white,
                                  ),
                                ),
                              ),
                            ),
                          ),
                          Container(
                            height: 40.0,
                            width: 2.0,
                            color: Colors.white,
                          ),
                          GestureDetector(
                            onTap: () {
                              setState(() {
                                isProjectManager = true;
                              });
                            },
                            child: Container(
                              width: MediaQuery.of(context).size.width / 2 - 18,
                              decoration: BoxDecoration(
                                borderRadius: const BorderRadius.only(
                                  topRight: Radius.circular(6.0),
                                  bottomRight: Radius.circular(6.0),
                                ),
                                color: isProjectManager
                                    ? hexToColor(lightBlueColor)
                                    : Colors.transparent,
                              ),
                              child: Center(
                                child: Text(
                                  "Projektmanager",
                                  style: TextStyle(
                                    color: Colors.white,
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(height: 20.0),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text(
                          'Sie haben bereits ein Konto?',
                          style: TextStyle(
                            color: Colors.white60,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        SizedBox(width: 2.0),
                        GestureDetector(
                          onTap: () {
                            Navigator.pop(context);
                          },
                          child: Text(
                            'Login',
                            style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
                Expanded(
                  child: Align(
                    alignment: FractionalOffset.bottomCenter,
                    child: Padding(
                      padding: EdgeInsets.only(bottom: 100.0),
                      child: GestureDetector(
                        onTap: () {
                          moveToRegistrationPasswordScreen(context);
                        },
                        child: Container(
                          height: 50.0,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(8.0),
                            border: Border.all(
                              color: Colors.white,
                              width: 2.0,
                            ),
                          ),
                          child: Center(
                            child: Text(
                              "Weiter",
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 25.0,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  moveToRegistrationPasswordScreen(BuildContext context) {
    // Navigator.pushNamed(context, 'registration_password');

    Navigator.of(context)
        .push(MaterialPageRoute(builder: (context) => RegistrationPasswordScreen(text: firstnameTextController.text.isNotEmpty ? firstnameTextController.text : "Rüdiger", isProjectManager: isProjectManager,)));
  }
}
