import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:visio_app/Screens/OTPVerificationScreen.dart';
import 'package:visio_app/constant/constant.dart';

class RegistrationPasswordScreen extends StatefulWidget {
  final String text;
  bool isProjectManager;
  RegistrationPasswordScreen({Key key, @required this.text, @required this.isProjectManager}) : super(key: key);
  @override
  _RegistrationPasswordScreenState createState() =>
      _RegistrationPasswordScreenState();
}

class _RegistrationPasswordScreenState
    extends State<RegistrationPasswordScreen> {
  final password = TextEditingController();
  final repeatPassword = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage("assets/images/background.png"),
              fit: BoxFit.cover,
            ),
          ),
          child: Padding(
            padding: const EdgeInsets.all(15.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(
                  height: 50.0,
                ),
                Text(
                  "Register",
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 40.0,
                      fontWeight: FontWeight.bold),
                ),
                SizedBox(height: 10.0),
                Text(
                  "Bitte registrieren Sie sich",
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 25.0,
                  ),
                ),
                SizedBox(height: 50.0),
                Column(
                  children: [
                    Container(
                      child: TextField(
                        // onChanged: (value) => isFieldsValid(),
                        controller: password,
                        keyboardType: TextInputType.text,
                        obscureText: true,
                        decoration: InputDecoration(
                          prefixIcon: Icon(
                            CupertinoIcons.lock_fill,
                            color: Colors.white,
                          ),
                          hintStyle: new TextStyle(
                            color: Colors.grey,
                            fontSize: 18.0,
                          ),
                          enabledBorder: UnderlineInputBorder(
                            borderSide:
                                BorderSide(color: Colors.white, width: 2.0),
                          ),
                          focusedBorder: UnderlineInputBorder(
                            borderSide:
                                BorderSide(color: Colors.white, width: 2.0),
                          ),
                          hintText: 'Passwort',
                        ),
                        style: TextStyle(
                          fontSize: 14.0,
                          color: Colors.white,
                        ),
                      ),
                    ),
                    SizedBox(height: 20.0),
                    Container(
                      child: TextField(
                        // onChanged: (value) => isFieldsValid(),
                        controller: repeatPassword,
                        keyboardType: TextInputType.text,
                        obscureText: true,
                        decoration: InputDecoration(
                          prefixIcon: Icon(
                            CupertinoIcons.lock_fill,
                            color: Colors.white,
                          ),
                          hintStyle: new TextStyle(
                            color: Colors.grey,
                            fontSize: 18.0,
                          ),
                          enabledBorder: UnderlineInputBorder(
                            borderSide:
                                BorderSide(color: Colors.white, width: 2.0),
                          ),
                          focusedBorder: UnderlineInputBorder(
                            borderSide:
                                BorderSide(color: Colors.white, width: 2.0),
                          ),
                          hintText: 'Passwort wiederholen',
                        ),
                        style: TextStyle(
                          fontSize: 14.0,
                          color: Colors.white,
                        ),
                      ),
                    ),
                  ],
                ),
                Expanded(
                  child: Align(
                    alignment: FractionalOffset.bottomCenter,
                    child: Padding(
                      padding: EdgeInsets.only(bottom: 100.0),
                      child: GestureDetector(
                        onTap: () {
                          moveToOTPVerificationScreen(context);
                        },
                        child: Container(
                          height: 50.0,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(8.0),
                            border: Border.all(
                              color: Colors.white,
                              width: 2.0,
                            ),
                          ),
                          child: Center(
                            child: Text(
                              "Registrieren",
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 25.0,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  moveToOTPVerificationScreen(BuildContext context) {

    Navigator.of(context)
        .push(MaterialPageRoute(builder: (context) => OTPVerificationScreen(text: widget.text, isProjectManager: widget.isProjectManager,)));
  }
}
