import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:pin_code_fields/pin_code_fields.dart';
import 'package:visio_app/Screens/ProjectManager/PackageScreen.dart';
import 'package:visio_app/constant/constant.dart';

class OTPVerificationScreen extends StatefulWidget {
  final String text;
  bool isProjectManager;
  OTPVerificationScreen(
      {Key key, @required this.text, @required this.isProjectManager})
      : super(key: key);
  @override
  _OTPVerificationScreenState createState() => _OTPVerificationScreenState();
}

class _OTPVerificationScreenState extends State<OTPVerificationScreen> {
  TextEditingController textEditingController =
      TextEditingController(); // ..text = "123456";
  StreamController<ErrorAnimationType> errorController;

  bool hasError = false;
  String currentText = "";
  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();
  final formKey = GlobalKey<FormState>();

  @override
  void initState() {
    errorController = StreamController<ErrorAnimationType>();
    super.initState();
  }

  @override
  void dispose() {
    errorController.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffoldKey,
      body: SingleChildScrollView(
        child: Container(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage("assets/images/background.png"),
              fit: BoxFit.cover,
            ),
          ),
          child: Padding(
            padding: const EdgeInsets.all(15.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(
                  height: 50.0,
                ),
                Text(
                  "Hallo " + widget.text ?? "Rüdiger",
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 40.0,
                      fontWeight: FontWeight.bold),
                ),
                SizedBox(height: 10.0),
                RichText(
                  textAlign: TextAlign.start,
                  text: new TextSpan(
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 30.0,
                        height: 1.4,
                        fontWeight: FontWeight.w200),
                    children: <TextSpan>[
                      new TextSpan(
                          text:
                              'um ein Bauobjekt hinzuzufügen, gib bitte den '),
                      new TextSpan(
                          text: 'fünfstelligen ',
                          style: new TextStyle(fontWeight: FontWeight.bold)),
                      new TextSpan(
                          text: 'numerischen Code',
                          style: new TextStyle(fontWeight: FontWeight.bold)),
                      TextSpan(text: ' ein.'),
                    ],
                  ),
                ),
                SizedBox(height: 50.0),
                Form(
                  key: formKey,
                  child: Padding(
                    padding: const EdgeInsets.symmetric(
                        vertical: 8.0, horizontal: 10),
                    child: PinCodeTextField(
                      appContext: context,
                      inputFormatters: [FilteringTextInputFormatter.digitsOnly],
                      pastedTextStyle: TextStyle(
                        color: Colors.green.shade600,
                        fontWeight: FontWeight.bold,
                      ),
                      length: 5,
                      obscureText: false,
                      obscuringCharacter: '*',
                      blinkWhenObscuring: true,
                      animationType: AnimationType.fade,
                      backgroundColor: Colors.transparent,
                      // validator: (v) {
                      //   if (v.length < 3) {
                      //     return "I'm from validator";
                      //   } else {
                      //     return null;
                      //   }
                      // },
                      pinTheme: PinTheme(
                        shape: PinCodeFieldShape.box,
                        borderRadius: BorderRadius.circular(5),
                        borderWidth: 2,
                        inactiveColor: Colors.white,
                        activeColor: Colors.white,
                        disabledColor: Colors.white,
                        inactiveFillColor: hexToColor(backgroundColor),
                        selectedColor: Colors.white,
                        selectedFillColor: hexToColor(backgroundColor),
                        fieldHeight: 50,
                        fieldWidth: 50,
                        activeFillColor: hexToColor(backgroundColor),
                      ),
                      cursorColor: Colors.black,
                      animationDuration: Duration(milliseconds: 300),
                      enableActiveFill: true,
                      errorAnimationController: errorController,
                      controller: textEditingController,
                      keyboardType: TextInputType.number,
                      textInputAction: TextInputAction.go,
                      boxShadows: [
                        BoxShadow(
                          offset: Offset(0, 1),
                          color: Colors.black12,
                          blurRadius: 10,
                        )
                      ],
                      onCompleted: (v) {
                        print("Completed");
                        print(currentText);
                        // gotoHomeScreen(context);
                      },
                      onChanged: (value) {
                        print(value);
                        setState(() {
                          currentText = value;
                        });
                      },
                      beforeTextPaste: (text) {
                        print("Allowing to paste $text");
                        //if you return true then it will show the paste confirmation dialog. Otherwise if false, then nothing will happen.
                        //but you can show anything you want here, like your pop up saying wrong paste format or etc
                        return true;
                      },
                    ),
                  ),
                ),
                Expanded(
                  child: Align(
                    alignment: FractionalOffset.bottomCenter,
                    child: Padding(
                      padding: EdgeInsets.only(bottom: 100.0),
                      child: GestureDetector(
                        onTap: () {
                          moveToNavBarScreen(context);
                        },
                        child: Container(
                          height: 50.0,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(8.0),
                            border: Border.all(
                              color: Colors.white,
                              width: 2.0,
                            ),
                          ),
                          child: Center(
                            child: Text(
                              "Bestätigen",
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 25.0,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  moveToNavBarScreen(BuildContext context) {
    widget.isProjectManager
        ? Navigator.of(context)
            .push(MaterialPageRoute(builder: (context) => PackageScreen(isFromStart: true,)))
        : Navigator.pushNamed(context, 'builder_nav');
  }
}
