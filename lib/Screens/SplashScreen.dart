import 'package:flutter/material.dart';
import 'package:visio_app/Screens/RegistrationScreen.dart';
import 'package:visio_app/constant/constant.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    super.initState();
    new Future.delayed(const Duration(seconds: 3), () {
      Navigator.pushReplacementNamed(context, 'login');
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage("assets/images/backgroundWhite.png"),
            fit: BoxFit.cover,
          ),
        ),
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child: Stack(
          children: [
            Center(
              child: Container(
                padding: const EdgeInsets.all(25.0),
                child: Image.asset(
                  "assets/images/LogoSplash.png",
                  fit: BoxFit.cover,
                ),
              ),
            ),
            Positioned(
              right: 0.0,
              left: 0.0,
              bottom: 100.0,
              child: Container(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      "by",
                      style: TextStyle(
                          color: hexToColor(greyColor),
                          fontSize: 40.0,
                          fontWeight: FontWeight.bold),
                    ),
                    SizedBox(width: 20.0),
                    Image.asset(
                      "assets/images/Logo2.png",
                      height: 50.0,
                      fit: BoxFit.cover,
                    ),
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
