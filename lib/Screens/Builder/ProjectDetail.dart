import 'dart:ffi';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:percent_indicator/circular_percent_indicator.dart';
import 'package:percent_indicator/linear_percent_indicator.dart';
import 'package:visio_app/Screens/Builder/DocumentScreens.dart';
import 'package:visio_app/Screens/Builder/InformationScreen.dart';
import 'package:visio_app/Screens/Builder/MilestoneScreen.dart';

class ProjectDetail extends StatefulWidget {
  @override
  _ProjectDetailState createState() => _ProjectDetailState();
}

class _ProjectDetailState extends State<ProjectDetail> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage("assets/images/background.png"),
              fit: BoxFit.cover,
            ),
          ),
          child: MediaQuery.removePadding(
            removeTop: true,
            context: context,
            child: ListView(
              // mainAxisAlignment: MainAxisAlignment.start,
              // crossAxisAlignment: CrossAxisAlignment.start,
              // mainAxisSize: MainAxisSize.min,
              children: [
                Padding(
                  padding: const EdgeInsets.only(
                    top: 50.0,
                    left: 10.0,
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      GestureDetector(
                        onTap: () {
                          Navigator.pop(context);
                        },
                        child: Container(
                          height: 30,
                          width: 30,
                          // decoration: BoxDecoration(
                          //     borderRadius: BorderRadius.circular(100),
                          //     border: Border.all(width: 3, color: Colors.black)),
                          child: Icon(
                            Icons.keyboard_arrow_left,
                            color: Colors.white,
                            size: 40.0,
                          ),
                        ),
                      ),
                      SizedBox(width: 20.0),
                      Text(
                        "LettestraBe 2",
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 30.0,
                            fontWeight: FontWeight.bold),
                      ),
                    ],
                  ),
                ),
                SizedBox(height: 20.0),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 20.0),
                  child: LinearPercentIndicator(
                    animation: true,
                    lineHeight: 25.0,
                    animationDuration: 2000,
                    percent: 0.6,
                    center: Text("60.0%"),
                    linearStrokeCap: LinearStrokeCap.roundAll,
                    progressColor: Colors.blue,
                  ),
                ),
                SizedBox(height: 20.0),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 20.0),
                  child: Text(
                    "Meilensteine",
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 25.0,
                        fontWeight: FontWeight.w300),
                  ),
                ),
                SizedBox(height: 20.0),
                Container(
                  height: 330,
                  child: ListView.separated(
                      separatorBuilder: (context, index) =>
                          SizedBox(height: 15.0),
                      shrinkWrap: true,
                      scrollDirection: Axis.horizontal,
                      itemCount: 5,
                      itemBuilder: (BuildContext context, int index) {
                        return _buildProjectView(index);
                      }),
                ),
                SizedBox(height: 20.0),
                Container(
                  height: 150.0,
                  padding: const EdgeInsets.all(20.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      GestureDetector(
                        onTap: () {
                          moveToDocScreen(context);
                        },
                        child: Container(
                          height: 150,
                          width: MediaQuery.of(context).size.width / 2 - 30,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(30),
                              border: Border.all(width: 1, color: Colors.grey)),
                          child: Stack(
                            children: [
                              Center(
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Icon(
                                      CupertinoIcons.doc,
                                      color: Colors.white,
                                      size: 50.0,
                                    ),
                                    SizedBox(height: 5.0),
                                    Text(
                                      "Dokumente",
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontSize: 20.0,
                                          fontWeight: FontWeight.w300),
                                    ),
                                  ],
                                ),
                              ),
                              // Positioned(
                              //   right: 0,
                              //   child: new Container(
                              //     padding: EdgeInsets.all(1),
                              //     decoration: new BoxDecoration(
                              //       color: Colors.red,
                              //       borderRadius: BorderRadius.circular(10),
                              //     ),
                              //     constraints: BoxConstraints(
                              //       minWidth: 20,
                              //       minHeight: 20,
                              //     ),
                              //     child: new Text(
                              //       '1',
                              //       style: new TextStyle(
                              //         color: Colors.white,
                              //         fontSize: 10,
                              //       ),
                              //       textAlign: TextAlign.center,
                              //     ),
                              //   ),
                              // )
                              Positioned(
                                top: 0,
                                right: 0,
                                child: Container(
                                  height: 20.0,
                                  width: 20.0,
                                  decoration: BoxDecoration(
                                    color: Colors.red,
                                    borderRadius: BorderRadius.circular(10.0),
                                  ),
                                  child: Center(
                                      child: Text(
                                    "1",
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 11.0,
                                        fontWeight: FontWeight.bold),
                                  )),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                      GestureDetector(
                        onTap: () {
                          moveToInfoScreen(context);
                        },
                        child: Container(
                          height: 150,
                          width: MediaQuery.of(context).size.width / 2 - 30,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(30),
                              border: Border.all(width: 1, color: Colors.grey)),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Icon(
                                Icons.info_outline,
                                color: Colors.white,
                                size: 50.0,
                              ),
                              SizedBox(height: 5.0),
                              Text(
                                "Informationen",
                                style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 20.0,
                                    fontWeight: FontWeight.w300),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  _buildProjectView(int index) {
    return Container(
      height: 350,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(20),
        color: Colors.transparent,
      ),
      child: Container(
        height: 350,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(20),
          color: Colors.transparent,
        ),
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 25.0),
              child: Container(
                height: 150,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(20.0),
                    topRight: Radius.circular(20.0),
                  ),
                  color: Colors.white,
                ),
                child: Container(
                  height: 150,
                  width: MediaQuery.of(context).size.width * 0.85,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(15.0),
                    color: Colors.blue,
                  ),
                  child: Image.asset(
                    "assets/images/Logo1.png",
                    height: 150.0,
                    fit: BoxFit.cover,
                  ),
                ),
              ),
            ),
            Container(
              width: MediaQuery.of(context).size.width * 0.85,
              height: 60,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(20.0),
                  bottomRight: Radius.circular(20.0),
                ),
                color: Colors.white,
              ),
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Column(
                      children: [
                        Text(
                          "Fertigstellung FuBbodenheizung",
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: 16.0,
                              fontWeight: FontWeight.bold),
                        ),
                        Text(
                          "Verlegen der leitungen im EG und 1.0G",
                          style: TextStyle(
                              color: Colors.grey,
                              fontSize: 14.0,
                              fontWeight: FontWeight.w300),
                        ),
                      ],
                    ),
                    SizedBox(width: 5.0),
                    GestureDetector(
                      onTap: () {
                        moveToMilestoneScreen(context);
                      },
                      child: Container(
                        height: 30,
                        width: 30,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(100),
                            border: Border.all(width: 3, color: Colors.black)),
                        child: Icon(
                          Icons.arrow_forward,
                          color: Colors.black,
                          size: 20.0,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Container(
              height: 70,
              width: 20,
              decoration: BoxDecoration(
                color: Colors.blue,
              ),
            ),
            Container(
              height: 50,
              width: MediaQuery.of(context).size.width,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(15),
                color: Colors.transparent,
              ),
              child: Padding(
                padding: index == 0
                    ? const EdgeInsets.only(left: 30.0)
                    : index == 4
                        ? const EdgeInsets.only(right: 30.0)
                        : const EdgeInsets.all(0),
                child: Container(
                  height: 50,
                  width: MediaQuery.of(context).size.width,
                  decoration: BoxDecoration(
                    borderRadius: index == 0
                        ? BorderRadius.only(
                            topLeft: Radius.circular(8.0),
                            bottomLeft: Radius.circular(8.0),
                          )
                        : index == 4
                            ? BorderRadius.only(
                                topRight: Radius.circular(8.0),
                                bottomRight: Radius.circular(8.0),
                              )
                            : BorderRadius.circular(0.0),
                    color: Colors.blue,
                  ),
                  child: Center(
                      child: Text(
                    "27.03.2020",
                    style: TextStyle(color: Colors.white, fontSize: 20),
                  )),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  moveToMilestoneScreen(BuildContext context) {
    // Navigator.pushNamed(context, 'builder_milestone');
    Navigator.of(context)
        .push(MaterialPageRoute(builder: (context) => MilestoneScreen()));
  }

  moveToDocScreen(BuildContext context) {
    // Navigator.pushNamed(context, 'builder_doc');
    Navigator.of(context)
        .push(MaterialPageRoute(builder: (context) => DocumentScreen()));
  }

  moveToInfoScreen(BuildContext context) {
    // Navigator.pushNamed(context, 'builder_info');
    Navigator.of(context)
        .push(MaterialPageRoute(builder: (context) => InformationScreen()));
  }
}
