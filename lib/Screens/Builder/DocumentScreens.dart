import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class DocumentScreen extends StatefulWidget {
  @override
  _DocumentScreenState createState() => _DocumentScreenState();
}

class _DocumentScreenState extends State<DocumentScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage("assets/images/background.png"),
            fit: BoxFit.cover,
          ),
        ),
        child: Column(
          children: [
            Container(
              // color: Colors.red,
              height: 170,
              child: Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.only(
                      top: 50.0,
                      left: 10.0,
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        GestureDetector(
                          onTap: () {
                            Navigator.pop(context);
                          },
                          child: Container(
                            height: 30,
                            width: 30,
                            // decoration: BoxDecoration(
                            //     borderRadius: BorderRadius.circular(100),
                            //     border: Border.all(width: 3, color: Colors.black)),
                            child: Icon(
                              Icons.keyboard_arrow_left,
                              color: Colors.white,
                              size: 40.0,
                            ),
                          ),
                        ),
                        SizedBox(width: 20.0),
                        Text(
                          "LettestraBe 2",
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 30.0,
                              fontWeight: FontWeight.bold),
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(
                        horizontal: 60.0, vertical: 20.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Icon(
                          CupertinoIcons.doc,
                          color: Colors.white,
                          size: 40.0,
                        ),
                        SizedBox(width: 10.0),
                        Text(
                          "Dokumente",
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 22.0,
                              fontWeight: FontWeight.w300),
                        ),
                        SizedBox(width: 5.0),
                        Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(
                              height: 20.0,
                              width: 20.0,
                              decoration: BoxDecoration(
                                color: Colors.red,
                                borderRadius: BorderRadius.circular(10.0),
                              ),
                              child: Center(
                                  child: Text(
                                "1",
                                style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 11.0,
                                    fontWeight: FontWeight.bold),
                              )),
                            ),
                            SizedBox(height: 15.0),
                          ],
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            Expanded(
              child: Padding(
                padding: const EdgeInsets.only(
                    top: 0.0, left: 15.0, right: 15.0),
                child: Container(
                  child: MediaQuery.removePadding(
                    removeTop: true,
                    context: context,
                    child: ListView.separated(
                        separatorBuilder: (context, index) =>
                            SizedBox(height: 15.0),
                        physics: BouncingScrollPhysics(),
                        padding: EdgeInsets.symmetric(vertical: 10.0),
                        scrollDirection: Axis.vertical,
                        shrinkWrap: true,
                        itemCount: 3,
                        itemBuilder: (BuildContext context, int index) {
                          return _buildDocumentView(index);
                        }),
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  _buildDocumentView(index) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20.0, vertical: 5.0),
      child: Container(
        height: 60.0,
        width: MediaQuery.of(context).size.width,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(5.0),
          border: Border.all(
              color: Colors.white, style: BorderStyle.solid, width: 1),
        ),
        child: FlatButton(
          onPressed: () {
            // moveToWindDateDetailScreen(context);
          },
          child: Text('Bauplan_EG.pdf',
              style: TextStyle(color: Colors.white, fontSize: 18.0)),
          textColor: Colors.white,
        ),
      ),
    );
  }
}
