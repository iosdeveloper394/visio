import 'package:flutter/material.dart';
import 'package:visio_app/constant/constant.dart';

class InformationScreen extends StatefulWidget {
  @override
  _InformationScreenState createState() => _InformationScreenState();
}

class _InformationScreenState extends State<InformationScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage("assets/images/background.png"),
              fit: BoxFit.cover,
            ),
          ),
          child: MediaQuery.removePadding(
            removeTop: true,
            context: context,
            child: ListView(
              children: [
                Padding(
                  padding: const EdgeInsets.only(
                    top: 50.0,
                    left: 10.0,
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      GestureDetector(
                        onTap: () {
                          Navigator.pop(context);
                        },
                        child: Container(
                          height: 30,
                          width: 30,
                          // decoration: BoxDecoration(
                          //     borderRadius: BorderRadius.circular(100),
                          //     border: Border.all(width: 3, color: Colors.black)),
                          child: Icon(
                            Icons.keyboard_arrow_left,
                            color: Colors.white,
                            size: 40.0,
                          ),
                        ),
                      ),
                      SizedBox(width: 20.0),
                      Text(
                        "LettestraBe 2",
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 30.0,
                            fontWeight: FontWeight.bold),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(
                      horizontal: 60.0, vertical: 20.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Icon(
                        Icons.info_outline,
                        color: Colors.white,
                        size: 40.0,
                      ),
                      SizedBox(width: 10.0),
                      Text(
                        "Informationen",
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 22.0,
                            fontWeight: FontWeight.w300),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(
                      horizontal: 20.0, vertical: 15.0),
                  child: Container(
                    height: 540,
                    padding: const EdgeInsets.all(20.0),
                    decoration: BoxDecoration(
                      color: hexToColor(greyColor).withOpacity(0.5),
                      borderRadius: BorderRadius.circular(5.0),
                      border: Border.all(
                          color: Colors.white,
                          style: BorderStyle.solid,
                          width: 1),
                    ),
                    child: ListView(
                      children: [
                        Text(
                          "Das Bauprojekt LettestraBe hat am 12.08.2019 begonnen. Die Fertigstellung erfolgt voraussichtlich im Sommer 2021.",
                          style: TextStyle(color: Colors.white, fontSize: 20.0),
                        ),
                        Text(
                          "\nAnsprechpartner\n",
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 22.0,
                              fontWeight: FontWeight.bold),
                        ),
                        Text(
                          "Sanitär",
                          style:
                              TextStyle(color: Colors.white70, fontSize: 22.0),
                        ),
                        Text(
                          "Ralf Müllar",
                          style:
                              TextStyle(color: Colors.white54, fontSize: 22.0),
                        ),
                        Text(
                          "0171 / 612 44 789\n",
                          style:
                              TextStyle(color: Colors.white54, fontSize: 22.0),
                        ),
                        Text(
                          "Heizung",
                          style:
                              TextStyle(color: Colors.white70, fontSize: 22.0),
                        ),
                        Text(
                          "Marcel Busch",
                          style:
                              TextStyle(color: Colors.white54, fontSize: 22.0),
                        ),
                        Text(
                          "0171 / 315 99 131\n",
                          style:
                              TextStyle(color: Colors.white54, fontSize: 22.0),
                        ),
                        Text(
                          "Architekt",
                          style:
                              TextStyle(color: Colors.white70, fontSize: 22.0),
                        ),
                        Text(
                          "Rüdiger Schulz",
                          style:
                              TextStyle(color: Colors.white54, fontSize: 22.0),
                        ),
                        Text(
                          "0171 / 910 43 123\n",
                          style:
                              TextStyle(color: Colors.white54, fontSize: 22.0),
                        ),
                        Text(
                          "Maler",
                          style:
                              TextStyle(color: Colors.white70, fontSize: 22.0),
                        ),
                        Text(
                          "Thomas Gossel",
                          style:
                              TextStyle(color: Colors.white54, fontSize: 22.0),
                        ),
                        Text(
                          "0171 / 647 28 823\n",
                          style:
                              TextStyle(color: Colors.white54, fontSize: 22.0),
                        ),
                        Text(
                          "Trockenbau",
                          style:
                              TextStyle(color: Colors.white70, fontSize: 22.0),
                        ),
                        Text(
                          "Maik Ginseng",
                          style:
                              TextStyle(color: Colors.white54, fontSize: 22.0),
                        ),
                        Text(
                          "0171 / 239 23 092\n",
                          style:
                              TextStyle(color: Colors.white54, fontSize: 22.0),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
