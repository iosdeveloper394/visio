import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:visio_app/constant/constant.dart';

class BottomSettings extends StatefulWidget {
  @override
  _BottomSettingsState createState() => _BottomSettingsState();
}

class _BottomSettingsState extends State<BottomSettings> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage("assets/images/background.png"),
              fit: BoxFit.cover,
            ),
          ),
          child: Padding(
            padding: const EdgeInsets.only(top: 15.0, left: 15.0, right: 15.0),
            child: MediaQuery.removePadding(
                removeTop: true,
                context: context,
                child: ListView(
                  children: [
                    SizedBox(
                      height: 50.0,
                    ),
                    Container(
                      padding: EdgeInsets.symmetric(
                          horizontal: 60.0, vertical: 20.0),
                      // color: Colors.red,
                      child: Image.asset(
                        "assets/images/LogoSplash.png",
                        fit: BoxFit.cover,
                      ),
                    ),
                    SizedBox(height: 20.0),
                    Container(
                      padding: const EdgeInsets.all(20.0),
                      width: MediaQuery.of(context).size.width,
                      decoration: BoxDecoration(
                        color: hexToColor(primaryColor),
                        borderRadius: BorderRadius.circular(10.0),
                        boxShadow: [
                          BoxShadow(
                              color: Colors.black.withOpacity(.3),
                              offset: Offset(2, 2),
                              blurRadius: 1,
                              spreadRadius: 1)
                        ],
                      ),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text("Visio - Supervision Technology",
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 18.0,
                                  fontWeight: FontWeight.w500)),
                          SizedBox(height: 10.0),
                          Text("visio@sts.archi",
                              style: TextStyle(
                                  color: Colors.grey,
                                  fontSize: 17.0,
                                  fontWeight: FontWeight.w300)),
                        ],
                      ),
                    ),
                    SizedBox(height: 20.0),
                    Container(
                      padding: const EdgeInsets.all(20.0),
                      width: MediaQuery.of(context).size.width,
                      decoration: BoxDecoration(
                        color: hexToColor(primaryColor),
                        borderRadius: BorderRadius.circular(10.0),
                        boxShadow: [
                          BoxShadow(
                              color: Colors.black.withOpacity(.3),
                              offset: Offset(2, 2),
                              blurRadius: 1,
                              spreadRadius: 1)
                        ],
                      ),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text("Kontakt zur Software",
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 18.0,
                                  fontWeight: FontWeight.w500)),
                          SizedBox(height: 10.0),
                          Text("Johannes Schörck",
                              style: TextStyle(
                                  color: Colors.white54,
                                  fontSize: 17.0,
                                  fontWeight: FontWeight.w400)),
                          SizedBox(height: 10.0),
                          Text("schoerch@sts.archi",
                              style: TextStyle(
                                  color: Colors.grey,
                                  fontSize: 17.0,
                                  fontWeight: FontWeight.w300)),
                          SizedBox(height: 10.0),
                          Text("+49 (0) 151 / 646 11 567",
                              style: TextStyle(
                                  color: Colors.grey,
                                  fontSize: 17.0,
                                  fontWeight: FontWeight.w300)),
                        ],
                      ),
                    ),
                    SizedBox(height: 20.0),
                    GestureDetector(
                      onTap: () {
                        _launchURL();
                      },
                      child: Container(
                        padding: const EdgeInsets.all(20.0),
                        width: MediaQuery.of(context).size.width,
                        decoration: BoxDecoration(
                          color: hexToColor(primaryColor),
                          borderRadius: BorderRadius.circular(10.0),
                          boxShadow: [
                            BoxShadow(
                                color: Colors.black.withOpacity(.3),
                                offset: Offset(2, 2),
                                blurRadius: 1,
                                spreadRadius: 1)
                          ],
                        ),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Text("Datenschutz und Rechtliches",
                                style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 18.0,
                                  fontWeight: FontWeight.w500,
                                )),
                            SizedBox(width: 10.0),
                            Icon(
                              Icons.link,
                              color: Colors.white,
                              size: 40.0,
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                )),
          ),
        ),
      ),
    );
  }

  _launchURL() async {
    const url = 'http://www.service-visio.com';
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }
}
