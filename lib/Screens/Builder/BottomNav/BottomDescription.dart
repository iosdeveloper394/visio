import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:visio_app/constant/constant.dart';

class BottomDescription extends StatefulWidget {
  @override
  _BottomDescriptionState createState() => _BottomDescriptionState();
}

class _BottomDescriptionState extends State<BottomDescription> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage("assets/images/background.png"),
            fit: BoxFit.cover,
          ),
        ),
        child: Column(
          children: [
            SizedBox(
              height: 50.0,
            ),
            Container(
              // color: Colors.red,
              height: 200,
              child: Container(
                padding: EdgeInsets.all(20.0),
                height: 150.0,
                // color: Colors.red,
                child: Image.asset("assets/images/Logo2.png"),
              ),
            ),
            Expanded(
              child: Padding(
                padding:
                    const EdgeInsets.only(top: 15.0, left: 15.0, right: 15.0),
                child: Container(
                  child: MediaQuery.removePadding(
                    removeTop: true,
                    context: context,
                    child: ListView(
                      children: [
                        RichText(
                          textAlign: TextAlign.start,
                          text: new TextSpan(
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 18.0,
                                height: 1.4,
                                fontWeight: FontWeight.w200),
                            children: <TextSpan>[
                              new TextSpan(
                                  text:
                                      'Die STS GmbH plant und begleilet Projekte in unterschiedlichen Massstäben und Themenfeldern seit 2004. Unsere Schwerpunkte liegen in '),
                              new TextSpan(
                                  text:
                                      'Architektur, Projektentwicklung, Mieterkoordination',
                                  style: new TextStyle(
                                      fontWeight: FontWeight.bold)),
                              new TextSpan(text: ' und '),
                              TextSpan(
                                  text: 'Projeksteuerung.',
                                  style: new TextStyle(
                                      fontWeight: FontWeight.bold)),
                            ],
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(20.0),
                          child: Column(
                            children: [
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceEvenly,
                                children: [
                                  Text(
                                    "Partner des TÜV Süd",
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 20.0,
                                        fontWeight: FontWeight.bold),
                                  ),
                                  Container(
                                    height: 80.0,
                                    // color: Colors.red,
                                    child: Image.asset(
                                        "assets/images/TUVSign.png"),
                                  ),
                                ],
                              ),
                              SizedBox(height: 20.0),
                              Container(
                                height: 2.0,
                                color: Colors.white,
                              ),
                              SizedBox(height: 20.0),
                              Text(
                                "Kontakt und Ansprechpartner",
                                style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 23.0,
                                  fontWeight: FontWeight.w300,
                                ),
                              ),
                            ],
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(10.0),
                          child: Column(
                            children: [
                              Container(
                                padding: const EdgeInsets.all(20.0),
                                width: MediaQuery.of(context).size.width,
                                decoration: BoxDecoration(
                                  color: hexToColor(primaryColor),
                                  borderRadius: BorderRadius.circular(10.0),
                                  boxShadow: [
                                    BoxShadow(
                                        color: Colors.black.withOpacity(.3),
                                        offset: Offset(2, 2),
                                        blurRadius: 1,
                                        spreadRadius: 1)
                                  ],
                                ),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text("Architekt Dipl. Ing. Rüdiger Schulz",
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontSize: 17.0,
                                            fontWeight: FontWeight.w400)),
                                    SizedBox(height: 10.0),
                                    Text("schulz@sts.archi",
                                        style: TextStyle(
                                            color: Colors.grey,
                                            fontSize: 16.0,
                                            fontWeight: FontWeight.w300)),
                                  ],
                                ),
                              ),
                              SizedBox(height: 20.0),
                              Container(
                                padding: const EdgeInsets.all(20.0),
                                width: MediaQuery.of(context).size.width,
                                decoration: BoxDecoration(
                                  color: hexToColor(primaryColor),
                                  borderRadius: BorderRadius.circular(10.0),
                                  boxShadow: [
                                    BoxShadow(
                                        color: Colors.black.withOpacity(.3),
                                        offset: Offset(2, 2),
                                        blurRadius: 1,
                                        spreadRadius: 1)
                                  ],
                                ),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text("Planung und Koordination",
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontSize: 17.0,
                                            fontWeight: FontWeight.w400)),
                                    SizedBox(height: 10.0),
                                    Text("Karina Treder",
                                        style: TextStyle(
                                            color: Colors.white54,
                                            fontSize: 17.0,
                                            fontWeight: FontWeight.w400)),
                                    SizedBox(height: 10.0),
                                    Text("treder@sts.archi",
                                        style: TextStyle(
                                            color: Colors.grey,
                                            fontSize: 16.0,
                                            fontWeight: FontWeight.w300)),
                                    SizedBox(height: 10.0),
                                    Text("planung@sts.archi",
                                        style: TextStyle(
                                            color: Colors.grey,
                                            fontSize: 16.0,
                                            fontWeight: FontWeight.w300)),
                                    SizedBox(height: 10.0),
                                    Text("+49 (0)381 / 44 44 09 14",
                                        style: TextStyle(
                                            color: Colors.grey,
                                            fontSize: 16.0,
                                            fontWeight: FontWeight.w300)),
                                  ],
                                ),
                              ),
                              SizedBox(height: 20.0),
                              GestureDetector(
                                onTap: () {
                                  _launchURL();
                                },
                                child: Container(
                                  padding: const EdgeInsets.all(20.0),
                                  width: MediaQuery.of(context).size.width,
                                  decoration: BoxDecoration(
                                    color: hexToColor(primaryColor),
                                    borderRadius: BorderRadius.circular(10.0),
                                    boxShadow: [
                                      BoxShadow(
                                          color: Colors.black.withOpacity(.3),
                                          offset: Offset(2, 2),
                                          blurRadius: 1,
                                          spreadRadius: 1)
                                    ],
                                  ),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: [
                                      Icon(
                                        Icons.public,
                                        color: Colors.white,
                                        size: 60.0,
                                      ),
                                      SizedBox(width: 10.0),
                                      Text("Zur Webseite",
                                          style: TextStyle(
                                            color: Colors.white,
                                            fontSize: 25.0,
                                            fontWeight: FontWeight.w500,
                                          )),
                                    ],
                                  ),
                                ),
                              ),
                              SizedBox(height: 20.0),
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  _launchURL() async {
    const url = 'http://www.sts.archi';
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }
}
