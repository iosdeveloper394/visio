import 'package:flutter/material.dart';
import 'package:visio_app/Screens/Builder/WindDetailScreen.dart';

class BottomWind extends StatefulWidget {
  @override
  _BottomWindState createState() => _BottomWindState();
}

class _BottomWindState extends State<BottomWind> {
  final itemCount = 3;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: SingleChildScrollView(
        scrollDirection: Axis.vertical,
        physics: ScrollPhysics(),
        child: Container(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage("assets/images/background.png"),
              fit: BoxFit.cover,
            ),
          ),
          child: Padding(
            padding: const EdgeInsets.only(
                top: 15.0, left: 15.0, right: 15.0, bottom: 80.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(
                  height: 50.0,
                ),
                Text(
                  "Wettermonitoring",
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 30.0,
                      fontWeight: FontWeight.bold),
                ),
                SizedBox(height: 10.0),
                Text(
                  "Laufende Projekte",
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 20.0,
                      fontWeight: FontWeight.w200),
                ),
                Expanded(
                  child: Container(
                    child: ListView.separated(
                        separatorBuilder: (context, index) =>
                            SizedBox(height: 15.0),
                        physics: BouncingScrollPhysics(),
                        padding: EdgeInsets.symmetric(vertical: 10.0),
                        scrollDirection: Axis.vertical,
                        shrinkWrap: true,
                        itemCount: itemCount,
                        itemBuilder: (BuildContext context, int index) {
                          return _buildProjectView(index, true);
                        }),
                  ),
                ),
                SizedBox(height: 10.0),
                Text(
                  "Abgeschlossene Projekte",
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 20.0,
                      fontWeight: FontWeight.w200),
                ),
                SizedBox(height: 10.0),
                Expanded(
                  child: Container(
                    child: ListView.separated(
                        separatorBuilder: (context, index) =>
                            SizedBox(height: 15.0),
                        physics: BouncingScrollPhysics(),
                        padding: EdgeInsets.symmetric(vertical: 10.0),
                        scrollDirection: Axis.vertical,
                        shrinkWrap: true,
                        itemCount: 2,
                        itemBuilder: (BuildContext context, int index) {
                          return _buildProjectView(index, false);
                        }),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  _buildProjectView(int index, bool isProcessing) {
    return ((index == itemCount - 1) && isProcessing)
        ? GestureDetector(
            onTap: () {
              //Navigator.of(context).pop();
            },
            child: new Align(
                alignment: Alignment.center,
                child: Container(
                  margin: EdgeInsets.all(10),
                  padding: EdgeInsets.all(10),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(100),
                      border: Border.all(width: 2, color: Colors.white)),
                  child: Icon(
                    Icons.add,
                    color: Colors.white,
                  ),
                )),
          )
        : Container(
            height: 250,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(20),
              color: Colors.white,
            ),
            child: Column(
              children: [
                Container(
                  height: 150,
                  width: MediaQuery.of(context).size.width,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20.0),
                    color: Colors.blue,
                  ),
                  child: Image.asset(
                    "assets/images/Logo1.png",
                    height: 150.0,
                    fit: BoxFit.cover,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(15.0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "LettestraBe 2",
                            style: TextStyle(
                                color: Colors.black,
                                fontSize: 18.0,
                                fontWeight: FontWeight.bold),
                          ),
                          Row(
                            children: [
                              GestureDetector(
                                onTap: () {},
                                child: Container(
                                  height: 30,
                                  width: 30,
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(100),
                                      border: Border.all(
                                          width: 3,
                                          color: isProcessing
                                              ? Colors.orange
                                              : Colors.green)),
                                  child: isProcessing
                                      ? Center(
                                          child: Text(
                                            "!",
                                            style: TextStyle(
                                                color: Colors.orange,
                                                fontSize: 16,
                                                fontWeight: FontWeight.bold),
                                          ),
                                        )
                                      : Icon(
                                          Icons.check,
                                          size: 20,
                                          color: Colors.green,
                                        ),
                                ),
                              ),
                              SizedBox(width: 10.0),
                              GestureDetector(
                                onTap: () {
                                  moveToWindDetailScreen(context);
                                },
                                child: Container(
                                  height: 30,
                                  width: 30,
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(100),
                                      border: Border.all(
                                          width: 3, color: Colors.black)),
                                  child: Icon(
                                    Icons.arrow_forward,
                                    color: Colors.black,
                                    size: 20.0,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                      Text(
                        "Progress: 65%",
                        style: TextStyle(
                            color: Colors.grey,
                            fontSize: 16.0,
                            fontWeight: FontWeight.w300),
                      ),
                    ],
                  ),
                )
              ],
            ),
          );
  }

  moveToWindDetailScreen(BuildContext context) {
    // Navigator.pushNamed(context, 'wind_detail');
        Navigator.of(context).push(MaterialPageRoute(builder: (context) => WindDetailScreen()));

  }
}
