import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:visio_app/Screens/Builder/WindDateDetail.dart';
import 'package:visio_app/constant/constant.dart';

class WindDetailScreen extends StatefulWidget {
  @override
  _WindDetailScreenState createState() => _WindDetailScreenState();
}

class _WindDetailScreenState extends State<WindDetailScreen> {
  ValueNotifier<DateTime> _dateTimeNotifier;
  var startDateController = TextEditingController();
  var endDateController = TextEditingController();

  String selectedStartTime;
  String selectedEndTime;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _dateTimeNotifier = ValueNotifier<DateTime>(DateTime.now());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage("assets/images/background.png"),
            fit: BoxFit.cover,
          ),
        ),
        child: MediaQuery.removePadding(
          removeTop: true,
          context: context,
          child: ListView(
            shrinkWrap: true,
            // mainAxisAlignment: MainAxisAlignment.start,
            // crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.only(
                  top: 50.0,
                  left: 10.0,
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    GestureDetector(
                      onTap: () {
                        Navigator.pop(context);
                      },
                      child: Container(
                        height: 30,
                        width: 30,
                        // decoration: BoxDecoration(
                        //     borderRadius: BorderRadius.circular(100),
                        //     border: Border.all(width: 3, color: Colors.black)),
                        child: Icon(
                          Icons.keyboard_arrow_left,
                          color: Colors.white,
                          size: 40.0,
                        ),
                      ),
                    ),
                    SizedBox(width: 10.0),
                    Text(
                      "LettestraBe 2",
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 30.0,
                          fontWeight: FontWeight.bold),
                    ),
                  ],
                ),
              ),
              Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 50.0, vertical: 5.0),
                child: Row(
                  children: [
                    Text(
                      "Wetterübersicht",
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 18.0,
                          fontWeight: FontWeight.w300),
                    ),
                    SizedBox(width: 10.0),
                    Icon(
                      CupertinoIcons.cloud,
                      color: Colors.white,
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(
                    horizontal: 20.0, vertical: 20.0),
                child: Column(
                  children: [
                    Container(
                      height: 55.0,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(5.0),
                        border: Border.all(
                            color: Colors.white,
                            style: BorderStyle.solid,
                            width: 1),
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Container(
                            width: MediaQuery.of(context).size.width * 0.5,
                            child: Padding(
                              padding: const EdgeInsets.only(left: 10.0),
                              child: TextField(
                                controller: startDateController,
                                readOnly: true,
                                textAlign: TextAlign.start,
                                style: TextStyle(
                                    fontSize: 16.0, color: Colors.white),
                                decoration: new InputDecoration(
                                    border: InputBorder.none,
                                    filled: true,
                                    hintStyle: TextStyle(
                                        color: Colors.white, fontSize: 16.0),
                                    hintText: "Start Date",
                                    fillColor: Colors.transparent),
                              ),
                            ),
                          ),
                          IconButton(
                            icon: Icon(
                              Icons.event,
                              color: Colors.grey,
                            ),
                            onPressed: () => showDatePicker(
                              context: context,
                              initialEntryMode: DatePickerEntryMode.calendar,
                              firstDate: DateTime(DateTime.now().year,
                                  DateTime.now().month, DateTime.now().day),
                              initialDate: DateTime(DateTime.now().year,
                                  DateTime.now().month, DateTime.now().day),
                              lastDate: DateTime(DateTime.now().year,
                                  DateTime.now().month + 1, DateTime.now().day),
                            ).then((DateTime dateTime) {
                              _dateTimeNotifier.value = DateTime(
                                  dateTime.year, dateTime.month, dateTime.day);
                              print(_dateTimeNotifier.value);
                              startDateController.text =
                                  "${_dateTimeNotifier.value.year}-${_dateTimeNotifier.value.month}-${_dateTimeNotifier.value.day}";
                              // showDialog(context: context, builder: (dialogContext) => myMethod(dialogContext),) ;
                              // _selectTime(context,1);
                            }),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(height: 20.0),
                    Container(
                      height: 55.0,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(5.0),
                        border: Border.all(
                            color: Colors.white,
                            style: BorderStyle.solid,
                            width: 1),
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Container(
                            // color: Colors.red,
                            width: MediaQuery.of(context).size.width * 0.5,
                            child: Padding(
                              padding: const EdgeInsets.only(left: 10.0),
                              child: TextField(
                                style: TextStyle(
                                    fontSize: 16.0, color: Colors.white),
                                controller: endDateController,
                                readOnly: true,
                                textAlign: TextAlign.start,
                                decoration: new InputDecoration(
                                    border: InputBorder.none,
                                    filled: true,
                                    hintStyle: TextStyle(
                                        color: Colors.white, fontSize: 16.0),
                                    hintText: "End Date",
                                    fillColor: Colors.transparent),
                              ),
                            ),
                          ),
                          IconButton(
                            icon: Icon(
                              Icons.event,
                              color: Colors.grey,
                            ),
                            onPressed: () => showDatePicker(
                              context: context,
                              initialEntryMode: DatePickerEntryMode.calendar,
                              firstDate: DateTime(DateTime.now().year,
                                  DateTime.now().month, DateTime.now().day),
                              initialDate: DateTime(DateTime.now().year,
                                  DateTime.now().month, DateTime.now().day),
                              lastDate: DateTime(DateTime.now().year,
                                  DateTime.now().month + 1, DateTime.now().day),
                            ).then((DateTime dateTime) {
                              _dateTimeNotifier.value = DateTime(
                                  dateTime.year, dateTime.month, dateTime.day);
                              print(_dateTimeNotifier.value);
                              endDateController.text =
                                  "${_dateTimeNotifier.value.year}-${_dateTimeNotifier.value.month}-${_dateTimeNotifier.value.day}";
                            }),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(height: 20.0),
                    Container(
                      height: 55.0,
                      width: MediaQuery.of(context).size.width,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(5.0),
                        border: Border.all(
                            color: Colors.white,
                            style: BorderStyle.solid,
                            width: 1),
                      ),
                      child: FlatButton(
                        onPressed: () {
                          moveToWindDateDetailScreen(context);
                        },
                        child:
                            Text('Done', style: TextStyle(color: Colors.white)),
                        textColor: Colors.white,
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(height: 10.0),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20.0),
                child: Container(
                  padding: const EdgeInsets.all(8.0),
                  width: MediaQuery.of(context).size.width,
                  decoration: BoxDecoration(
                    color: hexToColor(primaryColor),
                    borderRadius: BorderRadius.circular(5.0),
                    boxShadow: [
                      BoxShadow(
                          color: Colors.grey.withOpacity(.08),
                          offset: Offset(0, 0),
                          blurRadius: 20,
                          spreadRadius: 3)
                    ],
                  ),
                  child: RichText(
                    textAlign: TextAlign.start,
                    text: new TextSpan(
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 20.0,
                          fontWeight: FontWeight.w200),
                      children: <TextSpan>[
                        new TextSpan(text: 'Wählen Sie ein Datum aus, um die '),
                        new TextSpan(
                            text: 'Watterdetails',
                            style: new TextStyle(fontWeight: FontWeight.bold)),
                        new TextSpan(text: ' and '),
                        new TextSpan(
                            text: 'Wetterklassen',
                            style: new TextStyle(fontWeight: FontWeight.bold)),
                        new TextSpan(
                            text:
                                ' fur den gewünschten Zeitraum anzeigen zu lassen.'),
                      ],
                    ),
                  ),
                ),
              ),
              SizedBox(height: 15.0),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20.0),
                child: Container(
                  padding: const EdgeInsets.all(8.0),
                  width: MediaQuery.of(context).size.width,
                  decoration: BoxDecoration(
                    color: hexToColor(primaryColor),
                    borderRadius: BorderRadius.circular(5.0),
                    boxShadow: [
                      BoxShadow(
                          color: Colors.grey.withOpacity(.08),
                          offset: Offset(0, 0),
                          blurRadius: 20,
                          spreadRadius: 3)
                    ],
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "Klassifizierungen",
                        style: TextStyle(
                          fontSize: 25.0,
                          color: Colors.white,
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Column(
                          children: [
                            Container(
                              child: Row(
                                children: [
                                  Container(
                                    height: 50.0,
                                    width: 50.0,
                                    decoration: BoxDecoration(
                                      color: Colors.green,
                                      borderRadius: BorderRadius.circular(25.0),
                                      boxShadow: [
                                        BoxShadow(
                                            color: Colors.grey.withOpacity(.08),
                                            offset: Offset(0, 0),
                                            blurRadius: 20,
                                            spreadRadius: 3)
                                      ],
                                    ),
                                  ),
                                  SizedBox(width: 10.0),
                                  RichText(
                                    text: new TextSpan(
                                      style: new TextStyle(
                                        fontSize: 14.0,
                                        color: Colors.white,
                                      ),
                                      children: <TextSpan>[
                                        new TextSpan(
                                          text: 'Klasse 1 - ',
                                          style: new TextStyle(
                                              fontWeight: FontWeight.bold,
                                              color: Colors.white,
                                              fontSize: 16.0),
                                        ),
                                        new TextSpan(
                                            text:
                                                'Bauarbeiten ohne \nEinschränkungen möglich'),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            SizedBox(height: 15.0),
                            Container(
                              child: Row(
                                children: [
                                  Container(
                                    height: 50.0,
                                    width: 50.0,
                                    decoration: BoxDecoration(
                                      color: Colors.orange,
                                      borderRadius: BorderRadius.circular(25.0),
                                      boxShadow: [
                                        BoxShadow(
                                            color: Colors.grey.withOpacity(.08),
                                            offset: Offset(0, 0),
                                            blurRadius: 20,
                                            spreadRadius: 3)
                                      ],
                                    ),
                                  ),
                                  SizedBox(width: 10.0),
                                  RichText(
                                    text: new TextSpan(
                                      style: new TextStyle(
                                        fontSize: 14.0,
                                        color: Colors.white,
                                      ),
                                      children: <TextSpan>[
                                        new TextSpan(
                                          text: 'Klasse 2 - ',
                                          style: new TextStyle(
                                              fontWeight: FontWeight.bold,
                                              color: Colors.white,
                                              fontSize: 16.0),
                                        ),
                                        new TextSpan(
                                            text:
                                                'Bauarbeiten unter \nEinschränkungen möglich'),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            SizedBox(height: 15.0),
                            Container(
                              child: Row(
                                children: [
                                  Container(
                                    height: 50.0,
                                    width: 50.0,
                                    decoration: BoxDecoration(
                                      color: Colors.red,
                                      borderRadius: BorderRadius.circular(25.0),
                                      boxShadow: [
                                        BoxShadow(
                                            color: Colors.grey.withOpacity(.08),
                                            offset: Offset(0, 0),
                                            blurRadius: 20,
                                            spreadRadius: 3)
                                      ],
                                    ),
                                  ),
                                  SizedBox(width: 10.0),
                                  RichText(
                                    text: new TextSpan(
                                      style: new TextStyle(
                                        fontSize: 14.0,
                                        color: Colors.white,
                                      ),
                                      children: <TextSpan>[
                                        new TextSpan(
                                          text: 'Klasse 3 - ',
                                          style: new TextStyle(
                                              fontWeight: FontWeight.bold,
                                              color: Colors.white,
                                              fontSize: 16.0),
                                        ),
                                        new TextSpan(
                                            text:
                                                'Bauarbeiten nicht \n möglich'),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              SizedBox(height: 15.0),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20.0),
                child: Container(
                  padding: const EdgeInsets.all(8.0),
                  width: MediaQuery.of(context).size.width,
                  decoration: BoxDecoration(
                    color: hexToColor(primaryColor),
                    borderRadius: BorderRadius.circular(5.0),
                    boxShadow: [
                      BoxShadow(
                          color: Colors.grey.withOpacity(.08),
                          offset: Offset(0, 0),
                          blurRadius: 20,
                          spreadRadius: 3)
                    ],
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "Rechtssicherheit",
                        style: TextStyle(
                          fontSize: 25.0,
                          color: Colors.white,
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 10.0),
                        child: RichText(
                          textAlign: TextAlign.start,
                          text: new TextSpan(
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 22.0,
                                fontWeight: FontWeight.w200),
                            children: <TextSpan>[
                              new TextSpan(
                                  text:
                                      'Unsere Wetterdaten dienen lediglich der Übersicht and können '),
                              new TextSpan(
                                  text: 'nicht',
                                  style: new TextStyle(
                                      fontWeight: FontWeight.bold)),
                              new TextSpan(
                                  text:
                                      ' vor Gericht verwendet wernden. Um rechtssichere Daten des DWD zu erhalten, fragen Sie uns bitte an.'),
                            ],
                          ),
                        ),
                      ),
                      Container(
                      height: 40.0,
                      width: MediaQuery.of(context).size.width,
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(10.0),
                        border: Border.all(
                            color: Colors.white,
                            style: BorderStyle.solid,
                            width: 1),
                      ),
                      child: FlatButton(
                        onPressed: null,
                        child:
                            Text('Unverbindlich Anfragen', style: TextStyle(color: Colors.black)),
                        textColor: Colors.white,
                      ),
                    ),
                    ],
                  ),
                ),
              ),
              SizedBox(height: 20.0),
            ],
          ),
        ),
      ),
    );
  }

  moveToWindDateDetailScreen(BuildContext context) {
    // Navigator.pushNamed(context, 'wind_date_detail');
        Navigator.of(context).push(MaterialPageRoute(builder: (context) => WindDateDetail()));

  }
}
