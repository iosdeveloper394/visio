import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';
import 'package:visio_app/Screens/Builder/BottomNav/BottomDescription.dart';
import 'package:visio_app/Screens/Builder/BottomNav/BottomHome.dart';
import 'package:visio_app/Screens/Builder/BottomNav/BottomSetting.dart';
import 'package:visio_app/Screens/Builder/BottomNav/BottomWind.dart';
import 'package:visio_app/constant/constant.dart';

class BottomNav extends StatefulWidget {
  final int index;
  BottomNav({Key key, this.index}) : super(key: key);

  @override
  _BottomNavState createState() => _BottomNavState();
}

class _BottomNavState extends State<BottomNav> with TickerProviderStateMixin {
  int _currentIndex = 0;
  PersistentTabController _controller;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    widget.index != null ? _currentIndex = widget.index : print("test");
    _controller = PersistentTabController(initialIndex: 0);
  }

  List<PersistentBottomNavBarItem> _navBarsItems() {
    return [
      PersistentBottomNavBarItem(
        icon: Icon(CupertinoIcons.home),
        title: ("Home"),
        activeColorPrimary: CupertinoColors.white,
        inactiveColorPrimary: CupertinoColors.systemGrey,
      ),
      PersistentBottomNavBarItem(
        icon: Icon(CupertinoIcons.wind),
        title: ("Settings"),
        activeColorPrimary: CupertinoColors.white,
        inactiveColorPrimary: CupertinoColors.systemGrey,
      ),
      PersistentBottomNavBarItem(
        icon: Icon(Icons.notes),
        title: ("Settings"),
        activeColorPrimary: CupertinoColors.white,
        inactiveColorPrimary: CupertinoColors.systemGrey,
      ),
      PersistentBottomNavBarItem(
        icon: Icon(CupertinoIcons.settings),
        title: ("Settings"),
        activeColorPrimary: CupertinoColors.white,
        inactiveColorPrimary: CupertinoColors.systemGrey,
      ),
    ];
  }

  @override
  Widget build(BuildContext context) {
    return PersistentTabView(
      context,
      controller: _controller,
      screens: [
        BottomHome(),
        BottomWind(),
        BottomDescription(),
        BottomSettings(),
      ],
      items: _navBarsItems(),
      confineInSafeArea: true,
      backgroundColor: hexToColor(primaryColor), // Default is Colors.white.
      handleAndroidBackButtonPress: true, // Default is true.
      resizeToAvoidBottomInset:
          true, // This needs to be true if you want to move up the screen when keyboard appears. Default is true.
      stateManagement: true, // Default is true.
      hideNavigationBarWhenKeyboardShows:
          true, // Recommended to set 'resizeToAvoidBottomInset' as true while using this argument. Default is true.
      decoration: NavBarDecoration(
        borderRadius: BorderRadius.circular(10.0),
        colorBehindNavBar: Colors.transparent,
      ),
      popAllScreensOnTapOfSelectedTab: true,
      popActionScreens: PopActionScreensType.all,
      itemAnimationProperties: ItemAnimationProperties(
        // Navigation Bar's items animation properties.
        duration: Duration(milliseconds: 200),
        curve: Curves.ease,
      ),
      screenTransitionAnimation: ScreenTransitionAnimation(
        // Screen transition animation on change of selected tab.
        animateTabTransition: true,
        curve: Curves.ease,
        duration: Duration(milliseconds: 200),
      ),
      navBarStyle:
          NavBarStyle.style12, // Choose the nav bar style with this property.
    );
    // DefaultTabController(
    //   length: 4,
    //   initialIndex: _currentIndex,
    //   child: Scaffold(
    //     extendBody: true,
    //     bottomNavigationBar: menu(context),
    //     body: TabBarView(
    //       physics: NeverScrollableScrollPhysics(),
    //       children: [
    // BottomHome(),
    // BottomWind(),
    // BottomDescription(),
    // BottomSettings(),
    //       ],
    //     ),
    //   ),
    // );
  }

  Widget menu(BuildContext context) {
    return Container(
      height: 75.0,
      decoration: BoxDecoration(
        color: Colors.black12,
        // borderRadius: BorderRadius.only(
        //     topRight: Radius.circular(50), topLeft: Radius.circular(50)),
        // boxShadow: [
        //   BoxShadow(color: Colors.black38, spreadRadius: 0, blurRadius: 10),
        // ],
      ),
      child: TabBar(
        labelColor: Colors.white,
        unselectedLabelColor: Colors.black,
        indicatorSize: TabBarIndicatorSize.label,
        indicatorColor: Colors.white,
        labelStyle: TextStyle(
          fontSize: 11.0,
          color: Colors.white,
        ),
        unselectedLabelStyle: TextStyle(
          fontSize: 11.0,
          color: Colors.black26,
        ),
        tabs: [
          Tab(
            icon: Icon(
              Icons.home_filled,
              size: 28.0,
            ),
          ),
          Tab(
            icon: Icon(
              CupertinoIcons.wind,
              size: 28.0,
            ),
          ),
          Tab(
            icon: Icon(
              Icons.notes,
              size: 28.0,
            ),
          ),
          Tab(
            icon: Icon(
              Icons.settings,
              size: 28.0,
            ),
          ),
        ],
        onTap: (index) {
          setState(() {
            _currentIndex = index;
          });
        },
      ),
    );
  }
}
