import 'package:flutter/material.dart';
import 'package:visio_app/constant/constant.dart';

class MilestoneScreen extends StatefulWidget {
  @override
  _MilestoneScreenState createState() => _MilestoneScreenState();
}

class _MilestoneScreenState extends State<MilestoneScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage("assets/images/background.png"),
              fit: BoxFit.cover,
            ),
          ),
          child: MediaQuery.removePadding(
            removeTop: true,
            context: context,
            child: ListView(
              children: [
                Padding(
                  padding: const EdgeInsets.only(
                    top: 50.0,
                    left: 10.0,
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      GestureDetector(
                        onTap: () {
                          Navigator.pop(context);
                        },
                        child: Container(
                          height: 30,
                          width: 30,
                          // decoration: BoxDecoration(
                          //     borderRadius: BorderRadius.circular(100),
                          //     border: Border.all(width: 3, color: Colors.black)),
                          child: Icon(
                            Icons.keyboard_arrow_left,
                            color: Colors.white,
                            size: 40.0,
                          ),
                        ),
                      ),
                      SizedBox(width: 20.0),
                      Text(
                        "LettestraBe 2",
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 30.0,
                            fontWeight: FontWeight.bold),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(
                      horizontal: 62.0, vertical: 10.0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "Fertigstellung",
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 22.0,
                            fontWeight: FontWeight.w300),
                      ),
                      SizedBox(height: 5.0),
                      Text(
                        "FuBbodenheizung",
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 22.0,
                            fontWeight: FontWeight.w300),
                      ),
                      SizedBox(height: 20.0),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(
                      horizontal: 40.0, vertical: 15.0),
                  child: Container(
                    height: 540,
                    padding: const EdgeInsets.all(30.0),
                    decoration: BoxDecoration(
                      color: hexToColor(greyColor).withOpacity(0.5),
                      borderRadius: BorderRadius.circular(5.0),
                      border: Border.all(
                          color: Colors.white,
                          style: BorderStyle.solid,
                          width: 1),
                    ),
                    child: Text(
                      "Die FuBbodenheizung wurden im gesamten Erdgeschossbereich, sowie im 1. Obergeschoss verlegt. Alle Anschlüsse wurden mit den Hauptwasserleitungen verbunden.\n Bei Rückfragen melden Sie sich bitte bei Herrn Hauke unter der 0171/4531902",
                      style: TextStyle(color: Colors.white54, fontSize: 22.0),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
